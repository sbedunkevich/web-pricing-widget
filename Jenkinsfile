pipeline {
    options {
        disableConcurrentBuilds()
    }    

    agent { label 'frontend' }

    stages {
        stage('Initialization') {
            steps {
                script {
                    env.LIBVERSION= getLibVersion()
                    env.VERSION = sh (returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.REPOSITORY = 'StreamLoan/web-pricing-widget'
                    env.IS_PRODUCTION = isProduction(env.JOB_NAME)
                    // define branch depending on type of build: branch or pull request
                    env.BRANCH=env.CHANGE_BRANCH ?: env.BRANCH_NAME
                    // Define NAMESPACE and target load balancer name(LB_NAME)
                    if (env.IS_PRODUCTION == 'true') {
                        env.NAMESPACE='production'
                        env.LB_NAME='url-map-webapp-lb-production'
                      } else {
                        // Need to use lowercased branch name because of GCE bucket naming rules
                        // https://cloud.google.com/storage/docs/naming#object-considerations
                        // Also trim branch name to first 40 characters
                        env.NAMESPACE=cleanBranchName(env.BRANCH)
                        env.LB_NAME='url-map-webapp-lb-development'
                    }
                                        // Define BUILD_TARGET
                    if (env.NAMESPACE == 'production') {
                        env.BUILD_TARGET='prod'
                    } else if (env.NAMESPACE == 'master') {
                        env.BUILD_TARGET='stage'
                    } else {
                        env.BUILD_TARGET='dev'
                    }
                    sh 'env'
                    notifyBuild('STARTED')
                }
            }
        }
        stage('Install dependencies') {
            steps {
                sh 'yarn install --pure-lockfile'
            }
        }

        stage('Build') {
            steps {
                sh 'yarn build:${BUILD_TARGET}'
            }
        }
        stage('Create cloud resources') {
            when {
                expression {
                    is_bucket_exist = sh (returnStatus: true, script: 'gsutil ls | grep gs://webpricing-${NAMESPACE}')
                    return is_bucket_exist != 0
                }
            }
            steps {
                sh 'gsutil mb gs://webpricing-${NAMESPACE}'
                sh 'gsutil defacl ch -u AllUsers:R gs://webpricing-${NAMESPACE}'
                sh 'gsutil web set -m streamloan.min.js  gs://webpricing-${NAMESPACE}'
                sh 'gcloud compute backend-buckets create backend-bucket-webpricing-${NAMESPACE} --gcs-bucket-name webpricing-${NAMESPACE}'
                sh """
                    gcloud compute url-maps add-path-matcher ${LB_NAME} \
                      --default-backend-bucket backend-bucket-webpricing-${NAMESPACE} \
                      --path-matcher-name path-matcher-webpricing-${NAMESPACE} \
                      --new-hosts '${NAMESPACE}-lib.streamloan.io'
                """ 
            }
        }
        stage('Upload') {
            steps {
                sh 'gsutil -m cp -r build/. gs://webpricing-${NAMESPACE}'
            }
        }
      
    }
    post {
        failure {
            // Need to set status manually in case of job failure
            script {
                currentBuild.result = "FAILED"
            }
        }
        always {
            // Success or failure, always send notifications
            notifyBuild(currentBuild.result)
            // Analyze tests
            // junit 'StreamLoan-Test-Suite/test/reports/*.xml'
        }
    }
}

def notifyBuild(String buildStatus = 'STARTED') {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESS'

    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
    def summary = "${subject} (${env.BUILD_URL})"
    def channel = (env.IS_PRODUCTION == 'true') ? '#status-jenkins-prod' : '#status-jenkins-dev'

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'LIGHT YELLOW'
        colorCode = '#FFFFCC'
    } else if (buildStatus == 'UNSTABLE') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'SUCCESS') {
        color = 'GREEN'
        colorCode = '#00FF00'
    } else {
        color = 'RED'
        colorCode = '#FF0000'
    }

    if (buildStatus == 'UNSTABLE' || buildStatus == 'SUCCESS' ) {
        summary = summary + "\nEnvironment link: " + getWebappUrl()
    }
    slackSend (color: colorCode, message: summary, channel: channel)

    script {
        env.TICKET_NUMBER = sh (returnStdout: true, script: 'echo $BRANCH | grep -oP STREAM-[0-9]+ || true')
    }
    def jqlSearch = "project = STREAM AND id = ${env.TICKET_NUMBER}"
    step([$class: 'hudson.plugins.jira.JiraIssueUpdateBuilder',
        jqlSearch: jqlSearch,
        comment: summary
    ])
}

def cleanBranchName (String branchName) {
    return branchName.length()>40 ? branchName.substring(0,40).toLowerCase() : branchName.toLowerCase()
} 

def isProduction (String jobName) {
    return jobName.toLowerCase().startsWith("library-webpricing-production")
}

def getWebappUrl () {
    return env.IS_PRODUCTION == 'true' ? "production lib link" : "https://${env.NAMESPACE}-lib.streamloan.io"
}

def getLibVersion () {
    def props = readJSON file: 'package.json'
    return props.version
}
