### Installation for Finance Factor

AppId for Finance Factor `MC44NTEzNT`

##### Steps for the integration into the customer's site.

* Deployment domain name. We should update the origin for CORS.
  The acmehomeloans origin is `https://acmehomeloans.com` for instance.
* Create the new route for search results with an empty page or only header/footer
* Insert our widget library on each page where it will be necessary. In a common scenario insert it into all pages.

```
</body>
<script src="https://pricing-lib.streamloan.io"></script>
  <script type="text/javascript">
    StreamLoan.init({
      appId: "MC44NTEzNT",
      elementSelector: ".open_widget_button",
      widgetContainerId: "result",
      redirectPageURL: "search-loan-page",
      applyPageURL: "https://financefactorsmortgage.streamloan.io/invite.html?id=a67c8f459d0f6e6d08359ba4e8787b8ff44d3779",
      theme: {
        primaryColor: '#3498db',
        secondaryColor: '#3498dc',
        fontFamily: 'Lato, Google Sans, Lato, Arial',
        gradientFromColor: 'white',
        gradientToColor: '#3498db',
      }
    });
  </script>
</html>
```

### StreamLoan.init params

##### appId

MC44NTEzNT

##### elementSelector

This is class or id of an element (button for instance) where Pricing Widget onClick will be mounted.
examples: `.click-on-me-buttons`, `#someButtonId`

##### redirectPageURL

The customer should create a new route for widget result page.
if URL is `https://acmehomeloans.com/search` the redirectPageURL will be `search`

In this route, you should have one empty div with id and widget script as well.

```
<div id="result"></div>
...
</body>
<script src="./lib/streamloan.js"></script>
<script type="text/javascript">
    StreamLoan.init({...})
</script>
```

##### widgetContainerId

The CSS Id of empty div where the results will be rendered.

##### applyPageURL

The URL with User ID there the APPLY button will link
`https://financefactorsmortgage.streamloan.io/invite.html?id=a67c8f459d0f6e6d08359ba4e8787b8ff44d3779`

#### theme

Color theme for the widget.

### Additional info

You can create the button with URL for search presets.
All 4 params should be set up.

```
downPayment, homeCost, zipCode, loanType = purchase | refi
```

example:
https://acmehomeloans.com/search.html?downPayment=240000&homeCost=1200000&zipCode=90034&loanType=purchase
