import fetch from 'cross-fetch';

export const getApiUrl = () => {
  try {
    const url = `${CONFIG_API_URL}`; // eslint-disable-line
    return url;
  } catch (err) {
    return 'https://pricing.streamloan.io';
  }
};

// Universal API Call
const asyncApiCall = async (
  endpoint,
  appId,
  method = 'GET',
  data = null,
  options = {}
) => {
  try {
    const API_URL = getApiUrl();
    const fullUrl =
      endpoint.indexOf(API_URL) === -1 ? API_URL + endpoint : endpoint;
    let body = data ? JSON.stringify(data) : null;
    const { multipart } = options;

    let token;

    let headers = {};

    headers['app-id'] = appId;

    if (token) headers.Authorization = `Bearer ${token}`;

    if (multipart) {
      let formData = new FormData();
      const { file } = data;
      formData.append('avatar', file);
      body = formData;
    } else {
      headers['Content-Type'] = 'application/json; charset=utf-8';
    }

    const res = await fetch(fullUrl, {
      method: method,
      headers: headers,
      credentials: 'include',
      body: body
    });
    const json = await res.json();

    if (res.ok) return json;

    throw json;
  } catch (err) {
    console.log("API ERROR!", err); // eslint-disable-line
    throw err;
  }
};

export const getDetails = (appId, searchId, productId) =>
  asyncApiCall('/api/getdetails', appId, 'POST', {
    searchId,
    appId,
    productId
  });

export const getOptions = (appId) =>
  asyncApiCall(`/api/accounts/${appId}/options`);

export const searchResults = (appId, borrowerInformation) =>
  asyncApiCall('/api/search', appId, 'POST', { ...borrowerInformation, appId });

export const checkZipcode = (appId, zipCode) =>
  asyncApiCall('/api/checkzipcode', appId, 'POST', { zipCode, appId });
