import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { ProgressBar } from 'web-ui-components';
import { Header2 } from '../UI';

/*
const EmailContainer = styled.form`
  display: flex;
  justify-content: center;
`;

const EmailInput = styled.input`
  padding: 10px;
  border: 1px solid #ccc;
  border-radius: 6px 0 0 6px;
  outline: none;
  font-size: 16px;
  width: 240px;
  ine-height: 1.1;
  transition: all 0.4s ease;
  font-family: ${props => props.theme.fontFamily};

  &:focus {
    border-color: ${props => props.theme.primaryColor || 'cyan'};
    box-shadow: 0 0 5px 0 ${props => props.theme.primaryColor || 'cyan'};
  }
`;

const SingUpButton = styled.button`
  background-image: none;
  text-shadow: none;
  background-color: ${props => props.theme.primaryColor};
  color: #fff;
  border: 1px solid ${props => props.theme.secondaryColor};
  box-shadow: none;
  position: relative;
  font-size: 16px;
  font-weight: 400;
  min-height: 40px;
  margin: 0px;
  white-space: nowrap;
  padding: 0 15px;
  opacity: 1;
  cursor: pointer;
  border-radius: 0 6px 6px 0;
  outline: none
  font-family: ${props => props.theme.fontFamily};

  border-color: ${props => props.theme.primaryColor || 'cyan'};
  box-shadow: 0 0 5px 0 ${props => props.theme.primaryColor || 'cyan'};
`;
*/

const Status = styled.div`
  text-align: center;
  font-weight: 100;
  font-size: 14px;
  margin-top: 2em;
  height: 15px;
  color: ${props => props.theme.primaryColor};
`;

const Error = styled.span`
  color: #777;
  font-size: 12px;
`;

const ProgressBarContainer = styled.div`
  width: 50%;
  margin: 1em auto;
`;

// const wait = time => new Promise(resolve => setTimeout(resolve, time));

const messages = [
  { title: 'connecting', progress: 1 },
  { title: 'connecting', progress: 2 },
  { title: 'sending request', progress: 4 },
  { title: 'sending request', progress: 5 },
  { title: 'sending request', progress: 8 },
  { title: 'processing data', progress: 10 },
  { title: 'processing data', progress: 12 },
  { title: 'processing data', progress: 16 },
  { title: 'processing data', progress: 19 },
  { title: 'almost done', progress: 22 },
  { title: 'almost done', progress: 24 }
];
const MESSAGES_INTERVAL = 1000;

class AskEmail extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    searchForResult: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { store } = this.props;
    // this.emailInput.focus();
    store.email = `${btoa(Math.random(1)).slice(0, 8)}@ezy.loans`;
    this.signUp();
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  state = {
    error: null,
    messageIndex: 0,
    search: false,
    progress: 0
  };

  search = async () => {
    const { searchForResult } = this.props;

    await searchForResult();
  };

  loadingMessages = () => {
    const { messageIndex } = this.state;

    if (Math.random() < 0.5) return;

    if (messageIndex < messages.length) {
      this.setState({
        error: messages[messageIndex].title,
        progress: messages[messageIndex].progress,
        messageIndex: messageIndex + 1
      });
    } else {
      clearInterval(this.timer);
    }
  };

  signUp = async () => {
    const { store } = this.props;
    const errors = [];

    if (!store.loanPrice) {
      errors.push('Loan price is not valid');
    } else {
      this.timer = setInterval(this.loadingMessages, MESSAGES_INTERVAL);
    }

    // if (!store.isEmailValid) {
    //   errors.push('Enter valid email');
    // }

    if (errors.length > 0) {
      this.setState({ error: errors.join(', ') });
    }

    store.animateLoader = true;
    // this.setState({ error: 'Email Sent!' });
    // Emulate action for email sending...
    // await wait(2000);
    // this.setState({ error: 'Checking pricing engines...' });
    await this.search();
    store.animateLoader = false;
    store.signUp(store.email);
  };

  render() {
    const { store } = this.props;
    const { error } = this.state;

    /*
      <EmailContainer onSubmit={this.signUp}>
          <EmailInput
            type="email"
            placeholder="Email"
            innerRef={ref => {
              this.emailInput = ref;
            }}
            value={store.email}
            onChange={ref => {
              this.setState({ error: null });
              store.email = ref.target.value;
            }}
          />
          <SingUpButton type="submit">Search</SingUpButton>
        </EmailContainer>
    */

    return (
      <div>
        <Header2>Cheking pricing engines...</Header2>
        <Status>
          <ProgressBarContainer>
            <ProgressBar
              value={this.state.progress}
              colors={['#ccc', '#bbb']}
              total={25}
              height="2px"
            />
          </ProgressBarContainer>
          <Error>{error && error}</Error> {store.signUpStatus}
        </Status>
      </div>
    );
  }
}

export default AskEmail;
