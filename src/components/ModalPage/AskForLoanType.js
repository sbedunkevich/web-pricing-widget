import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { QuestionInput, SquareButton, media } from 'web-ui-components';
import { Header2 } from '../UI';
import {
  STATUS_ERROR,
  STATUS_OK,
  LOAN_TYPE_PURCHASE,
  LOAN_TYPE_REFI
} from '../const';

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 2em 0;
  button {
    outline: none !important;
  }
  ${media.mobile`
    margin: 1em 0;
  `};
`;

class AskForLoanType extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired
  };

  state = {
    checkingCode: false
  };

  static cantGoNext(store) {
    if (!store.isZipCodeServed && store.isZipCodeValid) {
      return {
        status: STATUS_ERROR,
        message: 'This ZIP Code is out of our service area'
      };
    }

    if (store.isZipCodeValid) {
      return { status: STATUS_OK };
    }

    return { status: STATUS_ERROR, message: 'Please enter a valid ZIP Code' };
  }

  render() {
    const { store, config } = this.props;
    const { checkingCode } = this.state;
    const { appId } = config;

    const isZipCodeValid = store.isZipCodeValid;
    const isZipCodeServed = store.isZipCodeServed;
    const cityName = store.zipData.city ? store.zipData.city : '';
    let zipCodeText = 'for the ZIP Code of';

    if (store.zipCode.length === 5 && !checkingCode) {
      if (isZipCodeValid) {
        zipCodeText = `for the ZIP Code of ${cityName}`;
        if (!isZipCodeServed) {
          zipCodeText = 'ZIP Code not served';
        }
      } else {
        zipCodeText = 'Enter a valid ZIP Code';
      }
    }

    return (
      <div>
        <Header2>This is Loan for</Header2>
        <ButtonContainer>
          <SquareButton
            active={store.loanType === LOAN_TYPE_PURCHASE}
            onClick={() => (store.loanType = LOAN_TYPE_PURCHASE)}
          >
            New Home
          </SquareButton>
          <SquareButton
            active={store.loanType === LOAN_TYPE_REFI}
            onClick={() => (store.loanType = LOAN_TYPE_REFI)}
          >
            Refi
          </SquareButton>
        </ButtonContainer>
        <Header2>{zipCodeText}</Header2>
        <QuestionInput
          onFieldValueChange={async zipCode => {
            store.zipCode = zipCode;
            this.setState({ checkingCode: true });
            await store.lookupZipCode(appId);
            this.setState({ checkingCode: false });
          }}
          placeholder="ZIP Code"
          isValid={true}
          value={store.zipCode}
        />
      </div>
    );
  }
}

export default AskForLoanType;
