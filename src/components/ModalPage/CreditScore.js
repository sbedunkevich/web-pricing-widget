import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { SelectControl } from 'web-ui-components';
import { Header2 } from '../UI';
import { creditScoreOptions } from '../const';

const Contaner = styled.div`
  line-height: 1;
`;

class CreditScore extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  };

  render() {
    const { store } = this.props;

    return (
      <Contaner>
        <Header2>What is your credit score?</Header2>

        <SelectControl
          options={creditScoreOptions}
          validation={() => false}
          value={store.creditScore}
          disabled={false}
          withShadow
          onChange={value => {
            if (value.value) store.creditScore = value.value;
          }}
        />
      </Contaner>
    );
  }
}

export default CreditScore;
