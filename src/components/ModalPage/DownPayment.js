import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CurrencyInput } from 'web-ui-components';
import { InputContainer, Header2 } from '../UI';

class DownPayment extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  };

  render() {
    const { store } = this.props;

    return (
      <div>
        <Header2>How much do you have for your down payment?</Header2>

        <InputContainer>
          <CurrencyInput
            prefix="$"
            value={store.downPayment}
            precision="0"
            autoFocus
            thousandSeparator=","
            onChangeEvent={(event, maskedvalue, floatvalue) =>
              (store.downPayment = floatvalue)
            }
          />
        </InputContainer>
      </div>
    );
  }
}

export default DownPayment;
