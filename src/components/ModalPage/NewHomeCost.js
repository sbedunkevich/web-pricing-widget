import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Header2, InputContainer } from '../UI';
import { CurrencyInput } from 'web-ui-components';
import {
  STATUS_ERROR,
  STATUS_OK,
  LOAN_TYPE_PURCHASE,
  LOAN_TYPE_REFI
} from '../const';

class NewHomeCost extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired
  };

  static cantGoNext(store) {
    if (store.loanPrice > 0) {
      return { status: STATUS_OK };
    }
    return { status: STATUS_ERROR, message: 'Loan Price not valid!' };
  }

  render() {
    const { store } = this.props;
    let message;

    switch (store.loanType) {
      case LOAN_TYPE_PURCHASE:
        message = 'How much will the new home cost?';
        break;
      case LOAN_TYPE_REFI:
        message = 'How much will the refinance cost?';
        break;
      default:
        break;
    }

    return (
      <div>
        <Header2>{message}</Header2>
        <InputContainer>
          <CurrencyInput
            prefix="$"
            value={store.homeCost}
            precision="0"
            autoFocus
            thousandSeparator=","
            onChangeEvent={(event, maskedvalue, floatvalue) =>
              (store.homeCost = floatvalue)
            }
          />
        </InputContainer>
      </div>
    );
  }
}

export default NewHomeCost;
