import { observer } from 'mobx-react';
import AskForLoanType from './AskForLoanType';
import NewHomeCost from './NewHomeCost';
import DownPayment from './DownPayment';
import CreditScore from './CreditScore';
import AskEmail from './AskEmail';

export default [
  observer(AskForLoanType),
  observer(NewHomeCost),
  observer(DownPayment),
  observer(CreditScore),
  observer(AskEmail)
];
