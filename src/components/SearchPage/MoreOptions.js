import React, { Component } from 'react';
import { observer } from 'mobx-react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  SelectControl,
  CheckBox,
  SmallCheckBox,
  media
} from 'web-ui-components';
import {
  propertyTypeOptions,
  militaryVeteranOptions,
  propertyUseOptions,
  defaultLoanTerms
} from '../const';

const loanTerms = (config) => {
  if (config.loanTerms) {
    return [{
      name: 'Select All',
      value: false,
      selected: false }
    ].concat(config.loanTerms.map(term =>
      ({ name: term.name, value: term.value, selected: false })
    ));
  }
  return defaultLoanTerms;
};

const MainContainer = styled.div`
  display: flex;
  flex: 1;
  transition: height 0.2s ease, transform 0.1s ease, opacity 0.1s ease;
  height: ${props => (props.open ? 'auto' : '0')};
  transform: ${props => (props.open ? 'scaleY(1)' : 'scaleY(0)')};
  opacity: ${props => (props.open ? '1' : '0')};
  transform-origin: center top;
  line-height: 1;

  ${media.mobile`
    flex-direction: column;
    width: 100%;
  `};
`;

const Container = styled.div`
  display: flex;
  ${media.mobile`
    flex-direction: column;
  `};
`;

const FormContainer = styled.div`
  display: flex;

  & > div {
    margin-right: 1em;
    margin-bottom: 0.5em;

    ${media.mobile`
    margin-right: 0em;
    margin-bottom: 1em;
  `};
  }
`;

const Label = styled.label`
  font-size: 12px;
  display: block;
  margin-bottom: 10px;
`;

const TypeContainer = styled.div`
  ${props => (props.width ? `width: ${props.width}px;` : 'width: 300px')};
  font-size: 16px;

  ${media.mobile`
    width: 100%;
  `};
`;

const CheckValuesContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding-left: 0em;
  margin-top: 22px;
  font-weight: 400;

  label {
    font-weight: 400;
  }

  ${media.mobile`
    padding-left: 0em;
    margin: 1em 0;
  `};
`;

const CheckLabel = styled.label`
  margin-left: 0.5em;
  font-size: 16px;
  cursor: pointer;
  user-select: none;
  margin-bottom: 0;
  font-weight: 400;
`;

const CheckItem = styled.div`
  margin-bottom: 7px;
  display: flex;
  align-items: center;
`;

const LoanTypeValue = styled.div`
  color: black;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow-y: hidden;
  margin-right: 25px;
  color: ${props =>
    props.hasNoValues && (props.theme.placeHolderColor || '#ccc')};
`;

class MoreOptions extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    widthForSelect: PropTypes.number
  };

  constructor(props) {
    super(props);
    const { store, config } = props;
    const { selectedLoanTerms } = store;

    const options = {};

    loanTerms(config).forEach(term => {
      if (
        selectedLoanTerms &&
        !!selectedLoanTerms.filter(i => i === term.value).length
      ) {
        options[term.name] = true;
      }
    });
    this.state = { options };
  }

  renderValue = () => {
    const { store, config } = this.props;
    let selectedCount = 0;
    let totalOptions = 0;
    let selectedNames = [];
    let loanTypeOptions = [];
    let value;
    let hasNoValues = false;

    // eslint-disable-next-line
    loanTerms(config).map((item, index) => {
      totalOptions += 1;
      if (this.state.options[item.name]) {
        selectedCount += 1;
        if (index) {
          // first lement dont pushed
          selectedNames.push(item.name);
          loanTypeOptions.push(item.value);
        }
      }
    });

    switch (selectedCount) {
      case 0:
        value = 'Select loan terms';
        hasNoValues = true;
        break;
      case totalOptions:
        value = 'All selected';
        break;
      default:
        value = selectedNames.join(', ');
    }

    store.selectedLoanTermsHumanized = value;
    store.selectedLoanTerms = loanTypeOptions;
    return <LoanTypeValue hasNoValues={hasNoValues}>{value}</LoanTypeValue>;
  };

  onClickHandler = (index, term) => () => {
    const { config } = this.props;
    const options = { ...this.state.options };

    if (index === 0) {
      // Select All
      loanTerms(config).map(
        item => (options[item.name] = !this.state.options[loanTerms(loanTerms)[0].name])
      );
    } else {
      options[term.name] = !this.state.options[term.name];
    }
    this.setState({ options });
  };

  render() {
    const { store, open, widthForSelect, config } = this.props;

    const loanTermsOptions = loanTerms(config).map((term, index) => ({
      value: index,
      label: (
        <CheckItem>
          <SmallCheckBox
            type="checkbox"
            checked={!!this.state.options[term.name]}
            onChange={event => {
              if (event) {
                const options = { ...this.state.options };
                options[term.name] = !!event.target.checked;

                if (index === 0) {
                  // Select All
                  loanTerms(config).map(
                    item => (options[item.name] = !!event.target.checked)
                  );
                }
                this.setState({ options });
              }
            }}
          />
          <CheckLabel
            onTouchStart={this.onClickHandler(index, term)}
            onClick={this.onClickHandler(index, term)}
          >
            {term.name}
          </CheckLabel>
        </CheckItem>
      )
    }));

    //
    // ToDo
    // Get pricing engine fron back-end API
    // and setup the hasAdvancedOptions options.
    //
    const hasAdvancedOptions = false;

    return (
      <MainContainer open={open} style={{ marginRight: 0 }}>
        <div style={{ flex: '1' }}>
          <Container>
            <FormContainer>
              <TypeContainer width={widthForSelect}>
                <Label>Property Type</Label>
                <SelectControl
                  name="propertyType"
                  options={propertyTypeOptions}
                  validation={() => false}
                  value={store.propertyType}
                  disabled={false}
                  withShadow
                  onChange={value => {
                    if (value.value) store.propertyType = value.value;
                  }}
                />
              </TypeContainer>
            </FormContainer>

            <FormContainer>
              <TypeContainer width={widthForSelect}>
                <Label>Military Veteran?</Label>
                <SelectControl
                  name="militaryVeteran"
                  options={militaryVeteranOptions}
                  validation={() => false}
                  value={store.militaryVeteran}
                  disabled={false}
                  withShadow
                  onChange={value => {
                    if (value.value) store.militaryVeteran = value.value;
                  }}
                />
              </TypeContainer>
            </FormContainer>
          </Container>
          <Container>
            <FormContainer>
              <TypeContainer width={widthForSelect}>
                <Label>Property Use</Label>
                <SelectControl
                  name="propertyUse"
                  options={propertyUseOptions}
                  validation={() => false}
                  value={store.propertyUse}
                  disabled={false}
                  withShadow
                  onChange={value => {
                    if (value.value) store.propertyUse = value.value;
                  }}
                />
              </TypeContainer>
            </FormContainer>
            <FormContainer>
              <TypeContainer width={widthForSelect}>
                <Label>Loan Terms</Label>
                <SelectControl
                  name="loanTerms"
                  options={loanTermsOptions}
                  validation={() => false}
                  closeOnSelect={false}
                  dontHightLightCurrent={true}
                  disabled={false}
                  withShadow
                  valueRenderer={this.renderValue}
                />
              </TypeContainer>
            </FormContainer>
          </Container>
        </div>
        <div>
          <CheckValuesContainer>
            <CheckItem>
              <CheckBox
                type="checkbox"
                name="firstTimeHomeBuyer"
                id="firstTimeHomeBuyer"
                checked={store.firstTimeHomeBuyer}
                onChange={event =>
                  (store.firstTimeHomeBuyer = event.target.checked)
                }
              />
              <CheckLabel htmlFor="firstTimeHomeBuyer">
                First Time Homebuyer
              </CheckLabel>
            </CheckItem>
            {hasAdvancedOptions && (
              <div>
                <CheckItem>
                  <CheckBox
                    type="checkbox"
                    name="foreclosure"
                    id="foreclosure"
                    checked={store.foreclosure}
                    onChange={event =>
                      (store.foreclosure = event.target.checked)
                    }
                  />
                  <CheckLabel htmlFor="foreclosure">Foreclosure</CheckLabel>
                </CheckItem>
                <CheckItem>
                  <CheckBox
                    type="checkbox"
                    name="selfEmployed"
                    id="selfEmployed"
                    checked={store.selfEmployed}
                    onChange={event =>
                      (store.selfEmployed = event.target.checked)
                    }
                  />
                  <CheckLabel htmlFor="selfEmployed">Self Employed?</CheckLabel>
                </CheckItem>
                <CheckItem>
                  <CheckBox
                    type="checkbox"
                    name="bankruptcy"
                    id="bankruptcy"
                    checked={store.bankruptcy}
                    onChange={event =>
                      (store.bankruptcy = event.target.checked)
                    }
                  />
                  <CheckLabel htmlFor="bankruptcy">Bankruptcy?</CheckLabel>
                </CheckItem>
              </div>
            )}
          </CheckValuesContainer>
        </div>
      </MainContainer>
    );
  }
}

export default observer(MoreOptions);
