import React, { Component } from 'react';
import styled from 'styled-components';
import currencyFormatter from 'currency-formatter';
import { ApplyButton, ShowOnXS, HideOnXS } from '../UI';
import { DisclosureRight } from './StyledComponents';
import { media } from 'web-ui-components';
import { convertToFixed } from './helpers';

const ResultItemContainer = styled.div`
  display: flex;
  padding: 1.5em 1em;
  margin-bottom: 3px;
  align-items: center;
  justify-content: space-around;
  transition: all 0.3s ease;
  background: white;
  border-radius: 3px;
  line-height: 1;
  ${props => props.topPadding && `padding-top: ${props.topPadding};`} button {
    outline: none !important;
  }

  ${media.mobile`
    padding: 15px 10px;
    margin: 5px 0;
    ${props => props.topPadding && `padding-top: ${props.topPadding};`}
    justify-content: space-between;
  `};

  box-shadow: 0px 0px 0px rgba(0, 0, 0, 0.1);
  transform: scale(1);
  z-index: 0;

  :hover {
    box-shadow: 5px 5px 40px rgba(0, 0, 0, 0.1);
    transform: scale(1.01);
    ${media.mobile`
       transform: scale(1);
    `} z-index: 1;
  }

  & > div {
    font-size: 27px;
    font-weight: 300;
    min-width: 100px;
    margin-right: 1em;
    items-align: center;
    ${media.mobile`
      font-size: 16px;
      font-weight: 500;
      width: 100%;
      display: flex;
      min-width: auto;
      justify-content: center;
      margin-right: 0;
    `};
  }
`;

const DetailsLargeContainer = styled.div`
  width: 220px;
  display: flex;

  & > button:first-child {
    margin-right: 32px;
  }

  ${media.mobile`
    display: none;
  `};
`;

const DetailsButton = styled.button`
  color: ${props => props.theme.primaryColor};
  border: none;
  background: none;
  padding: 2px 0px;
  font-size: 14px;
  outline: none;
  border: none;
  cursor: pointer;

  :hover {
    border-bottom: 1px solid ${props => props.theme.primaryColor};
  }
`;

const SortItem = styled.div`
  position: absolute;
  margin-top: -50px;
  font-size: 16px;
  line-height: 1;

  ${media.mobile`
    font-size: 10px;
    margin-top: -35px;
  `};
`;

//
// Why do not used stateless component?
// const ResultItem = ({...propps}) => ...
// Answer:
// react-scroll don't support statless component animations
//
class ResultItem extends Component {
  render() {
    const {
      totalPayment,
      rate,
      apr,
      closingCost,
      viewDetails,
      onPressApply,
      itemId,
      index,
      headerItems
    } = this.props;

    return (
      <div id={itemId}>

        <HideOnXS>
          <ResultItemContainer topPadding={index === 0 ? '3.5em' : false}>
            <div>
              {index === 0 && <SortItem>{headerItems[0]}</SortItem>}
              {currencyFormatter.format(totalPayment, {
                code: 'USD',
                precision: 0
              })}
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[1]}</SortItem>}
              {rate.toFixed(4)}%
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[2]}</SortItem>}
              {convertToFixed(apr)}
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[3]}</SortItem>}
              {currencyFormatter.format(closingCost, {
                code: 'USD',
                precision: 0
              })}
            </div>
            <div>
              {index === 0 && (
                <SortItem>
                  <div>
                  LOAN DETAILS
                  </div>
                </SortItem>
              )}
              <DetailsLargeContainer>
                <DetailsButton onClick={viewDetails}>View Details</DetailsButton>
                <ApplyButton onClick={onPressApply}>Get Started</ApplyButton>
              </DetailsLargeContainer>
            </div>
          </ResultItemContainer>
        </HideOnXS>

        <ShowOnXS>
          <ResultItemContainer topPadding={index === 0 ? '3em' : null}>
            <div>
              {index === 0 && <SortItem>{headerItems[0]}</SortItem>}
              <span onClick={viewDetails}>
                {currencyFormatter.format(totalPayment, {
                  code: 'USD',
                  precision: 0
                })}
              </span>
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[1]}</SortItem>}
              <span onClick={viewDetails}>
                {rate.toFixed(4)}%
              </span>
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[2]}</SortItem>}
              <span onClick={viewDetails}>
                {convertToFixed(apr)}
              </span>
            </div>
            <div>
              {index === 0 && <SortItem>{headerItems[3]}</SortItem>}
              <span onClick={viewDetails}>
                {currencyFormatter.format(closingCost, {
                  code: 'USD',
                  precision: 0
                })}
              </span>
              <DisclosureRight onClick={viewDetails} />
            </div>
          </ResultItemContainer>
        </ShowOnXS>
      </div>
    );
  }
}

export default ResultItem;
