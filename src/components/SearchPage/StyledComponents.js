import styled from 'styled-components';
import { media, SvgColors } from 'web-ui-components';
import ArrowDown from '../UI/arrow-down.svg';
import Chevron from '../UI/chevron.svg';

const SearchForm = styled.form`
  z-index: 1;
  position: relative;
  background: white;
  border-radius: 8px;
  line-height: 1;
  padding: 0px;
  display: flex;
  flex-direction: column;
  margin: 2em 0;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);

  input {
    border-radius: 6px !important;
    font-weight: 200;
  }

  button {
    outline: none !important;
  }

  ${media.mobile`
    position: fixed;
    top: -2em;
    overflow: auto;
    box-shadow: none;
    display: block;
    z-index: 1000000;
    left: 0;
    right: 0;
    height: 0;
    transition: all 0.4s ease;
    border-radius: 0px;
    border: none;
    ${props => props.showEdit && 'height: 100vh; top: -2em;' }
  `};
`;

const SearchFormContainer = styled.div`
  display: flex;
  padding: 30px;
  padding-bottom: 0;

  & > div {
    margin-right: 1em;
  }

  ${media.mobile`
    flex-direction: column;
    padding: 1em;

    & > div {
      margin-right: 0;
    }
  `};
`;

const SearchFormContainerXS = styled(SearchFormContainer)`
  display: none;
  ${media.mobile`
    display: flex;
    flex-direction: row;
    padding-top: 1em;
    padding-bottom: 10em;
    justify-content: space-between;
  `};
`;

const AdvancedForm = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
  padding: 0 30px;
  padding-bottom: 10px;

  ${media.mobile`
    padding: 0 15px;
    justify-content: center;
  `};

  & > div {
    margin-right: 1em;
    ${media.mobile`
      margin-right: 0;
    `};
  }
`;

const ShowMoreButton = styled.button`
  background: none;
  color: ${props => props.theme.primaryColor};
  border: none;
  cursor: pointer;
  padding: 2px 0px;
  outline: none;
  font-size: 12px;
  outline: none !important;
  border-bottom: 1px solid ${props => props.theme.primaryColor};
`;

const Label = styled.label`
  font-size: 12px;
  display: block;
  margin-bottom: 10px;
  white-space: nowrap;

  ${media.mobile`
    margin-top: 10px;
  `};
`;

const CancelButton = styled.button`
  border: 1px solid ${props => props.theme.primaryColor};
  background: white;
  color: ${props => props.theme.primaryColor};
  border-radius: 6px;
  padding: 5px 10px;
  font-size: 16px;
  outline: none;
  height: 40px;
  font-weight: 400;
  align-self: flex-start;
  cursor: pointer;
  outline: none !important;
  transition: all 0.4s ease;
`;

const SubmitButton = styled.button`
  background-color: ${props => props.theme.primaryColor};
  color: white;
  border-radius: 6px;
  padding: 5px 10px;
  font-size: 16px;
  outline: none;
  border: none;
  height: 40px;
  font-weight: 400;
  align-self: flex-end;
  cursor: pointer;
  outline: none !important;
  transition: all 0.4s ease;
  :disabled {
    color: ${props => (props.red ? 'red' : props.theme.primaryColor)};
    background-color: white;
    border: 1px solid ${props => (props.red ? 'red' : props.theme.primaryColor)};
  }

  ${media.mobile`
    padding-left: ${props => (props.fetching ? '35px' : '10px')};
  `};
`;

const ResultForm = styled.div`
  background: none;
  border-radius: 8px;
  display: flex;
  padding: 0;
  margin: 2em 0;
  flex-direction: column;
`;

const ResulFormHeader = styled.div`
  background: rgba(0, 0, 0, 0.05);
  border: 1px solid rgba(0, 0, 0, 0.1);
  color: #777;
  padding: 10px;
  display: flex;
  align-items: center;
  font-size: 14px;
  border-radius: 3px 3px 0 0;
  padding-left: 27px;

  ${media.mobile`
    padding-left: 10px;
    font-size: 12px;
    justify-content: space-around;
  `};

  & > div {
    width: 152px;
    ${media.tablet`
      width: 173px;
    `} ${media.mobile`
      width: 25%;
      justify-content: center;
    `};
  }
`;

const LoanTypeContainer = styled.div`
  width: 220px;
  font-size: 16px;
  ${media.mobile`
    width: 100%;
  `};
`;

const CreditScoreContainer = styled.div`
  width: 280px;
  font-size: 16px;
  ${media.mobile`
    width: 100%;
  `};
`;

const ShowMoreContainer = styled.div`
  width: 145px;
  ${media.mobile`
    width: auto;
  `};
`;

const LoanPrice = styled.div`
  color: ${props => (props.error ? 'red' : '#bbb')};
  font-size: 12px;
  display: flex;
  position: absolute;
  margin-top: 1em;
  ${props => props.width && `width: ${props.width}px;`};
  max-width: 260px;
  margin-right: 0 !important;
  ${media.mobile`
    display: none;
  `};
`;

const LoanPriceLeft = styled.div`
  position: relative;
  width: 80px;
  height: 20px;
  flex: 1;

  &:after {
    content: "";
    position: absolute;
    right: 7px;
    height: 9px;
    width: calc(50% - 7px);
    border-radius: 0 0 0 4px;
    border: 1px solid ${props => (props.error ? 'red' : '#bbb')};
    border-top: none;
    border-right: none;
  }
`;

const LoanPriceRight = styled.div`
  position: relative;
  width: 80px;
  height: 20px;
  flex: 1;

  &:after {
    content: "";
    position: absolute;
    left: 7px;
    height: 9px;
    width: calc(50% - 7px);
    border-radius: 0 0 4px 0;
    border: 1px solid ${props => (props.error ? 'red' : '#bbb')};
    border-top: none;
    border-left: none;
  }
`;

const ErrorContainer = styled.div`
  margin: 1em 0;
  font-size: 14px;
  color: red;
  text-align: center;
  font-weight: 700;
`;

const SortedHeader = styled.div`
  cursor: pointer;
  user-select: none;
  text-transform: uppercase;
  display: flex;

  :hover {
    color: ${props => props.theme.primaryColor};
  }

  & > div {
    background: url(${ArrowDown}) top left no-repeat;
    background-position-y: ${props => (props.down ? '6px' : '1px')};
    margin-left: 5px;
    transition: transform 0.2s;
    display: inline-block;
    width: 10px;
    height: 15px;
    z-index: -1;
    margin-top: ${props => (props.down ? '-2px' : '-3px')};
    transform: ${props => (props.down ? 'rotate(0deg)' : 'rotate(-180deg)')};
    ${media.mobile`
      margin-right: 5px;
      background-position-y: ${props => (props.down ? '3px' : '3px')};
      margin-top: ${props => (props.down ? '-3px' : '-3px')};
    `};
  }
`;

const NotFound = styled.div`
  padding: 3em 0;
  color: #1e1e1e;
  font-size: 16px;
  font-weight: 100;
  text-align: center;
  font-family: ${props => props.theme.fontFamily};

  h1 {
    font-size: 22px;
    font-family: ${props => props.theme.fontFamily};
    font-weight: 400;
  }

  h2 {
    font-size: 20px;
    font-family: ${props => props.theme.fontFamily};
    font-weight: 300;
    padding-top: 1em;
    padding-bottom: 2em;
  }
`;

const ZipCode = styled.div`
  position: absolute;
  margin-top: 8px;
  font-size: 12px;
  color: ${props => (props.error ? 'red' : props.theme.primaryColor)};
  ${media.mobile`
    display: none;
  `};
`;

const ZipCodeXS = styled.div`
  margin: 0.5em 0 0.5em 0;
  font-size: 12px;
  display: none;
  color: ${props => (props.error ? 'red' : props.theme.primaryColor)};
  ${media.mobile`
    display: block;
  `};
`;

const ProgressBarContainer = styled.div`
  overflow: hidden;
  border-radius: 10px 10px 0 0;
  transition: all 0.3s ease;
  opacity: ${props => (props.show ? '1' : '0')};
  ${media.mobile`
    display: none;
  `};
`;

const DetailsViewContainer = styled.div`
  position: relative;
  height: 70vh;
  line-height: 1;
`;

const DetailsViewContent = styled.div`
  position: relative;
  height: 50vh;
  overflow-x: hidden;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
  padding: 0 3em;
  line-height: 200%;
  ${media.mobile`
    padding: 0 1.5em;
  `};
`;

const GetStarted = styled.div`
  position: relative;
  bottom: 0px
  height: 20vh;
  display: flex;
  align-items: center;
  overflow: hidden;
  justify-content: center;
  line-height: 100%;
  border-top: 1px solid #ccc;
`;

const GetStartedContent = styled.div`
  padding: 0;
  display: flex;
  flex-direction: column;
  line-height: 1;
  align-items: center;
`;

const GetStartedTitle = styled.div`
  color: #1e1e1e;
  font-size: 16px;
  margin-bottom: 10px;
`;

const GetStartedSubTitle = styled.div`
  color: #1e1e1e;
  font-size: 16px;
  font-weight: bold;
  margin-top: 10px;
`;

const Disclosure = styled.div`
  margin: 1em;
  font-size: 11px;
  color: #777;
  display: ${props => (props.visible ? 'block' : 'none')};
`;

const DetailsHeader = styled.div`
  font-weight: 100;
  font-size: 20px;
  color: #1e1e1e;
  margin: 1em 0;

  ${media.mobile`
    margin-top: 0;
    margin-bottom: 0.5em;
  `};
`;

const DetailsItem = styled.div`
  display: flex;
  transition: all 0.2s;
  ${props =>
    props.hasData && 'border-bottom: 0.5px dashed #eee; cursor: pointer'};
  margin-left: 1em;

  ${media.mobile`
    margin-left: 0;
    flex-direction: column;
  `};

  & > div:first-child {
    color: #848484;
    font-size: 14px;
    text-transform: uppercase;
    width: 60%;

    ${media.mobile`
      font-size: 14px;
      min-width: 100%;
    `};
  }

  & > div:nth-child(2) {
    font-size: 18px;
    font-weight: 500;
  }
`;

const CollapsedList = styled.div`
  margin: ${props => (props.open ? '1em 0' : '0')};
  display: ${props => (props.open ? 'block' : 'none')};
  margin-left: 1em;
`;

const DisclosureRight = styled.div`

  background: url(${props =>
    SvgColors(Chevron, { primaryColor: props.theme.primaryColor })})
  top left no-repeat;
  background-position-x: 12px;
  background-position-y: 1px;
  display: inline-block;
  outline: none !important;
  width: 23px;
  height: auto;
`;

const OpenCloseListButton = styled.div`
  background: url(${props =>
    SvgColors(Chevron, { primaryColor: props.theme.primaryColor })})
    top left no-repeat;
  transition: transform 0.2s;
  cursor: pointer;
  background-position-x: 8px;
  background-position-y: 8px;
  display: inline-block;
  outline: none !important;
  width: 23px;
  height: auto;
  transform: ${props => (props.open ? 'rotate(-90deg)' : 'rotate(90deg)')};
  ${props => props.open && 'margin-left: 16px;'};
`;

const LoaderConainter = styled.div`
  position: fixed;
  z-index: 10000;
  width: 100px;
  height: 20px;
  top: calc(50% - 20px);
  left: calc(50% - 60px);
  padding: 10px;
  display: none;
  ${media.mobile`
    display: flex;
  `};
  background: rgba(0, 0, 0, 0.3);
  border-radius: 10px;
  font-size: 13px;
  font-weight: 500;
  align-items: center;
  justify-content: center;
  color: white;
  transition: all 0.4s ease;
  user-select: none;
  pointer-events: none;
  opacity: ${props => (props.fetchingProducts ? '1.0' : '0')};
  transform: ${props => (props.fetchingProducts ? 'scale(1)' : 'scale(0)')};
`;

export {
  AdvancedForm,
  CreditScoreContainer,
  CancelButton,
  DisclosureRight,
  ErrorContainer,
  Label,
  LoaderConainter,
  LoanPrice,
  LoanPriceLeft,
  LoanPriceRight,
  LoanTypeContainer,
  OpenCloseListButton,
  ResulFormHeader,
  ResultForm,
  SearchForm,
  SearchFormContainer,
  SearchFormContainerXS,
  ShowMoreButton,
  ShowMoreContainer,
  SortedHeader,
  SubmitButton,
  CollapsedList,
  DetailsHeader,
  DetailsItem,
  DetailsViewContent,
  DetailsViewContainer,
  Disclosure,
  GetStarted,
  GetStartedContent,
  GetStartedSubTitle,
  GetStartedTitle,
  NotFound,
  ProgressBarContainer,
  ZipCode,
  ZipCodeXS,
};
