export const convertToFixed = value => {
  try {
    return `${value.toFixed(4)}%`;
  } catch (error) {
    return 'N/A';
  }
};
