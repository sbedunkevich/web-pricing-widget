import React, { Component } from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { autorun } from 'mobx';
import currencyFormatter from 'currency-formatter';
import {
  isEmpty as _isEmpty,
  orderBy as _orderBy,
  forEach as _forEach
} from 'lodash';
import FlipMove from 'react-flip-move';
import {
  CurrencyInput,
  PopUp,
  ProgressBar,
  SelectControl,
  Spinner
} from 'web-ui-components';
// import Scroll from 'react-scroll';
import {
  loanTypeOptions,
  creditScoreOptions,
  STATUS,
  SORTED_FIELDS
} from '../const';
import MoreOptions from './MoreOptions';
import ResultItem from './ResultItem';
import {
  Page,
  Content,
  Header1,
  Header2,
  InputContainer,
  BigApplyButton,
  ShowOnXS,
  HideOnXS
} from '../UI';
import {
  AdvancedForm,
  CreditScoreContainer,
  CancelButton,
  ErrorContainer,
  Label,
  LoaderConainter,
  LoanPrice,
  LoanPriceLeft,
  LoanPriceRight,
  LoanTypeContainer,
  OpenCloseListButton,
  ResultForm,
  SearchForm,
  SearchFormContainer,
  SearchFormContainerXS,
  ShowMoreButton,
  ShowMoreContainer,
  SortedHeader,
  SubmitButton,
  CollapsedList,
  DetailsHeader,
  DetailsItem,
  DetailsViewContent,
  DetailsViewContainer,
  Disclosure,
  GetStarted,
  GetStartedContent,
  GetStartedSubTitle,
  GetStartedTitle,
  NotFound,
  ProgressBarContainer,
  ZipCode,
  ZipCodeXS
} from './StyledComponents';
import { convertToFixed } from './helpers';

const CenterContainer = styled.div`
  text-align: center;
  margin-bottom: 3em;
`;

const ElipsisText = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const ChooseRateConteiner = styled.div`
  margin-top: 1em;
  text-align: center;
`;

const ChooseRate = styled.div`
  text-align: center;
  font-size: 22px;
  margin-top: 1em;
  margin-bottom: 2em;
`;

const PER_PAGE = 4;

// const Element = Scroll.Element;
// const scroller = Scroll.scroller;

const SordedField = ({ name, status, clickHandler }) => (
  <SortedHeader onClick={clickHandler} down={status.order === 'asc'}>
    {name} {status.order && <div />}
  </SortedHeader>
);

class SearchPage extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    searchForResult: PropTypes.func.isRequired
  };

  state = {
    showMoreOptions: false,
    openApply: false,
    openDetails: false,
    itemDetails: {},
    sortBy: [`${SORTED_FIELDS[0].field}`],
    sortOrder: ['asc'],
    sortToHuman: `${SORTED_FIELDS[0].field} ascending`,
    showClearButton: true,
    hideDisclosure: false,
    progress: 0,
    productNamesShow: {},
    loanPriceWidth: null,
    widthForSelect: null,
    showProgress: false,
    showEdit: false,
    sortedFields: [
      {
        name: SORTED_FIELDS[0].name,
        field: SORTED_FIELDS[0].field,
        status: STATUS[2]
      },
      {
        name: SORTED_FIELDS[1].name,
        field: SORTED_FIELDS[1].field,
        status: STATUS[1]
      },
      {
        name: SORTED_FIELDS[2].name,
        field: SORTED_FIELDS[2].field,
        status: STATUS[1]
      },
      {
        name: SORTED_FIELDS[3].name,
        field: SORTED_FIELDS[3].field,
        status: STATUS[1]
      }
    ]
  };

  toggleShowMore = () =>
    this.setState({ showMoreOptions: !this.state.showMoreOptions });

  loadingMessages = () => {
    let { progress } = this.state;
    if (progress > 100) {
      return;
    }
    progress += parseInt(Math.random() * 5, 10);
    this.setState({ progress });
  };

  onSubmit = async event => {
    const { searchForResult } = this.props;

    if (event) event.preventDefault();

    this.timer = setInterval(this.loadingMessages, 1000);

    await searchForResult();

    clearInterval(this.timer);
    this.setState({ progress: 0 });

    /*
    scroller.scrollTo('scrollToElement', {
      duration: 1000,
      delay: 100,
      smooth: true,
      offset: 0
    });*/
  };

  logOut = () => {
    const { store } = this.props;
    store.logOut();
  };

  viewDetails = item => () => {
    const { itemDetails } = this.state;
    const { productId } = itemDetails;

    const { store, config } = this.props;
    const { getDetails } = store;
    const { appId } = config;

    getDetails(appId, store.searchId, productId, this.updateDimensions);

    this.setState({
      openDetails: true,
      itemDetails: item
    });
  };

  updateDimensions = () => {
    const loanPriceWidth =
      this.downPaymentRef.offsetWidth + this.purchasePriceRef.offsetWidth + 16;
    let widthForSelect = (this.searchFormContainerRef.offsetWidth - 208) / 2;
    widthForSelect -= 46;
    this.setState({ loanPriceWidth, widthForSelect });
    if (this.getStartedDiv) {
      const height = this.getStartedDiv.clientHeight;
      if (height < 170) {
        this.setState({ hideDisclosure: true });
      } else {
        this.setState({ hideDisclosure: false });
      }
    }
  };

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
    this.onSubmit();
    this.handler = autorun(() => {
      const { store, config } = this.props;
      store.updateSearchURL(config);
      if (store.forseSearch) {
        store.forseSearch = false;
        this.onSubmit();
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
    if (this.handler) {
      this.handler();
      this.handler = null;
    }
  }

  addDataToURL = url => {
    const { store } = this.props;
    const { loanType, zipData, homeCost, downPayment } = store;
    const { state, zip } = zipData;
    const jsonData = {
      loanType,
      state,
      zip,
      homeCost,
      downPayment
    };

    if (loanType === 'Refi') {
      jsonData.loanType = 'refinance';
    }

    const prefix = url.search(/\?/) === -1 ? '?' : '&';
    const urlParams = Object.keys(jsonData)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(jsonData[k])}`)
      .join('&');

    const urlWithData = `${url}${prefix}${urlParams}`;
    return urlWithData;
  };

  renderApply = () => {
    const { config } = this.props;
    return (
      <PopUp
        poped={true}
        closePopup={() => this.setState({ openApply: false })}
        noPadding
        title={'Sign Up'}
      >
        <iframe
          src={this.addDataToURL(config.applyPageURL)}
          width="800"
          height="600"
          style={{ border: 'none' }}
        >
          iFrames not supported!
        </iframe>
      </PopUp>
    );
  };

  itemValuePresenter = item => {
    switch (item.type) {
      case 'money':
        return currencyFormatter.format(item.value, {
          code: 'USD',
          precision: 0
        });
      case 'fixed-percent':
        return convertToFixed(item.value);
      default:
        return item.value;
    }
  };

  renderDetailsContent = sections => {
    if (_isEmpty(sections)) {
      return null;
    }

    return sections.map((section, sectionIndex) => (
      <div key={sectionIndex}>
        <DetailsHeader>{section.title}</DetailsHeader>
        {section.items.map((item, itemIndex) => (
          <div key={`${sectionIndex}-${itemIndex}`}>
            <DetailsItem
              hasData={!_isEmpty(item.data)}
              onClick={() =>
                this.setState({
                  [`isOpen${sectionIndex}-${itemIndex}`]: !this.state[
                    `isOpen${sectionIndex}-${itemIndex}`
                  ]
                })
              }
            >
              <div>{item.name}</div>
              <div>{this.itemValuePresenter(item)}</div>
              {!_isEmpty(item.data) && (
                <OpenCloseListButton
                  open={this.state[`isOpen${sectionIndex}-${itemIndex}`]}
                >
                  &nbsp;
                </OpenCloseListButton>
              )}
            </DetailsItem>

            {!_isEmpty(item.data) && (
              <CollapsedList
                open={this.state[`isOpen${sectionIndex}-${itemIndex}`]}
              >
                {!_isEmpty(item.data) &&
                  item.data.map((dataItem, dataIndex) => (
                    <DetailsItem
                      key={`${sectionIndex}-${itemIndex}-${dataIndex}`}
                    >
                      <ElipsisText>{dataItem.Description}</ElipsisText>
                      <div>{dataItem.Amount}</div>
                    </DetailsItem>
                  ))}
              </CollapsedList>
            )}
          </div>
        ))}
      </div>
    ));
  };

  renderLoader = (fetchingProducts, theme) => {
    const { progress } = this.state;
    return (
      <LoaderConainter fetchingProducts={fetchingProducts}>
        <ProgressBar
          value={progress}
          total={100}
          height="4px"
          colors={[theme.primaryColor]}
        />
      </LoaderConainter>
    );
  };

  renderDetails = () => {
    const { itemDetails, hideDisclosure } = this.state;
    const {
      productName,
      aliasProductName,
      sections // New universal responce for details
    } = itemDetails;

    const { config } = this.props;
    const { applyPageURL } = config;

    return (
      <PopUp
        poped={true}
        closePopup={() => this.setState({ openDetails: false })}
        noPadding
        title={aliasProductName || productName}
      >
        <DetailsViewContainer>
          <DetailsViewContent>
            {this.renderDetailsContent(sections)}
          </DetailsViewContent>
          <GetStarted innerRef={node => (this.getStartedDiv = node)}>
            <GetStartedContent>
              <HideOnXS>
                <GetStartedTitle>Get started on this loan</GetStartedTitle>
              </HideOnXS>

              <HideOnXS>
                <BigApplyButton
                  onClick={() =>
                    this.setState({ openApply: true, openDetails: false })
                  }
                >
                  Get Started
                </BigApplyButton>
              </HideOnXS>
              <ShowOnXS>
                <a href={this.addDataToURL(applyPageURL)} target="_blank">
                  <BigApplyButton>Get Started</BigApplyButton>
                </a>
              </ShowOnXS>

              {config.phoneNumber && (
                <GetStartedSubTitle>
                  or call: {config.phoneNumber}
                </GetStartedSubTitle>
              )}
              <HideOnXS>
                {config.showDisclamer && (
                  <Disclosure visible={!hideDisclosure}>
                    Disclosure: This is not a loan approval or a commitment to
                    lend.<br />
                    Mortgage rates and loan costs are estimates based on the
                    info<br />
                  </Disclosure>
                )}
              </HideOnXS>
            </GetStartedContent>
          </GetStarted>
        </DetailsViewContainer>
      </PopUp>
    );
  };

  showMore = (key, size) => {
    const { productNamesShow } = this.state;

    if (!productNamesShow[key]) {
      return size > PER_PAGE;
    }
    return productNamesShow[key].slice < size;
  };

  sortProducts = (products, sortedFields) => {
    const { productNamesShow } = this.state;
    const group = {};
    const renderGroups = [];

    products.forEach(product => {
      const groupName = product.aliasProductName || product.productName;

      if (!group[groupName]) {
        group[groupName] = [];
      }
      group[groupName].push(product);
    });

    _forEach(group, (value, key) => {
      group[key] = _orderBy(value, this.state.sortBy, this.state.sortOrder);
    });

    _forEach(group, (value, key) => {
      const headerItems = sortedFields.map(item => (
        <SordedField
          clickHandler={this.toggleStatus(item.field)}
          key={item.field}
          {...item}
        />
      ));
      renderGroups.push(
        <div key={key}>
          <Header2>{key}</Header2>
          <ResultForm>
            <FlipMove staggerDurationBy="30" duration={500} typeName="div">
              {value
                .slice(
                  0,
                  productNamesShow[key] ? productNamesShow[key].slice : PER_PAGE
                )
                .map((item, i) => {
                  const itemKey = `${item.productId}-${item.apr}-${
                    item.totalPayment
                  }`;

                  return (
                    <ResultItem
                      index={i}
                      headerItems={headerItems}
                      itemId={itemKey}
                      key={itemKey}
                      {...item}
                      onPressApply={() =>
                        this.setState({ openApply: true, openDetails: false })
                      }
                      viewDetails={this.viewDetails(item)}
                    />
                  );
                })}
            </FlipMove>
          </ResultForm>

          {this.showMore(key, value.length) && (
            <CenterContainer>
              <ShowMoreButton
                onClick={() => {
                  const { productNamesShow: _productNamesShow } = this.state;
                  const nextSlice = _productNamesShow[key]
                    ? _productNamesShow[key].slice + PER_PAGE
                    : PER_PAGE * 2;

                  const nextProductNamesShow = {
                    ..._productNamesShow,
                    ...{ [key]: { slice: nextSlice } }
                  };
                  this.setState({ productNamesShow: nextProductNamesShow });
                }}
              >
                Show More
              </ShowMoreButton>
            </CenterContainer>
          )}
        </div>
      );
    });

    return renderGroups;
  };

  toggleStatus = field => () => {
    const sortedFields = [...this.state.sortedFields];

    sortedFields.forEach(f => {
      if (f.field === field) {
        f.status = STATUS[f.status.next]; // eslint-disable-line
      } else {
        f.status = STATUS[1]; // eslint-disable-line
      }
    });

    const sortBy = [];
    const sortOrder = [];
    const sortValues = [];

    sortedFields.forEach(s => {
      if (s.status.order) {
        sortBy.push(s.field);
        sortOrder.push(s.status.order);
        sortValues.push(`${s.name} ${s.status.humanValue}`);
      }
    });

    const sortToHuman = sortValues.join(', then ');

    this.setState({
      sortedFields,
      sortBy,
      sortOrder,
      sortToHuman,
      showClearButton: true
    });
  };

  clearSort = () => {
    const sortBy = [];
    const sortOrder = [];
    this.setState({
      sortedFields: [
        {
          name: SORTED_FIELDS[0].name,
          field: SORTED_FIELDS[0].field,
          status: STATUS[1]
        },
        {
          name: SORTED_FIELDS[1].name,
          field: SORTED_FIELDS[1].field,
          status: STATUS[1]
        },
        {
          name: SORTED_FIELDS[2].name,
          field: SORTED_FIELDS[2].field,
          status: STATUS[1]
        },
        {
          name: SORTED_FIELDS[3].name,
          field: SORTED_FIELDS[3].field,
          status: STATUS[1]
        }
      ],
      sortBy,
      sortOrder,
      sortToHuman: 'relevance',
      showClearButton: false
    });
  };

  renderIneligible = products =>
    products.map(({ Name, Reason }) => (
      <div>
        <div>{Name}</div>
        <div dangerouslySetInnerHTML={{ __html: Reason }} />
      </div>
    ));

  render() {
    const { store, config } = this.props;
    const {
      showMoreOptions,
      openDetails,
      sortedFields,
      openApply,
      loanPriceWidth,
      widthForSelect
    } = this.state;
    const { appId, theme, applyPageURL } = config;
    const {
      products,
      fetchingProducts,
      fetchingProductsError,
      isZipCodeValid,
      isZipCodeServed,
      loanPrice,
      // selectedLoanTermsHumanized,
      selectedLoanTerms,
      zipData: { city, state }
    } = store;

    const isFormValid =
      isZipCodeValid &&
      isZipCodeServed &&
      loanPrice &&
      !_isEmpty(selectedLoanTerms);

    let zipCodeText;
    let zipCodeHeader;

    const isPurchase = store.loanType === 'Purchase';

    if (isZipCodeValid) {
      zipCodeText = `${city} (${state})`;
      zipCodeHeader = `Your customized rates for ${city} (${state})`;
      if (!isZipCodeServed) {
        zipCodeText = 'ZIP Code currently not served';
        zipCodeHeader = 'Your customized rates';
      }
    } else {
      zipCodeText = 'Enter valid ZIP Code';
      zipCodeHeader = 'Your customized rates';
    }

    this.loanAmmount = isPurchase
      ? store.homeCost - store.downPayment
      : store.downPayment;

    return (
      <Page>
        {this.renderLoader(fetchingProducts, theme)}
        {openDetails && this.renderDetails()}
        {openApply && this.renderApply()}
        <Content>
          <Header1>{zipCodeHeader}</Header1>

          <ShowOnXS>
            <ChooseRateConteiner>
              <ShowMoreButton type="button" onClick={()=>this.setState({ showEdit: true })}>Edit Search</ShowMoreButton>
              <ChooseRate>Choose a rate to get started</ChooseRate>
            </ChooseRateConteiner>
          </ShowOnXS>
          <SearchForm onSubmit={this.onSubmit} noValidate showEdit={this.state.showEdit}>
            <ProgressBarContainer show={fetchingProducts}>
              <ProgressBar
                value={1}
                animationTime={1}
                animationType="linear"
                colors={[
                  theme.gradientFromColor,
                  theme.gradientToColor,
                  theme.gradientFromColor
                ]}
                animated={fetchingProducts}
                total={1}
                height="2px"
              />
            </ProgressBarContainer>
            <SearchFormContainer
              innerRef={node => (this.searchFormContainerRef = node)}
            >
              <LoanTypeContainer>
                <Label>Loan Type</Label>
                <SelectControl
                  name="loanType"
                  options={loanTypeOptions}
                  validation={() => false}
                  value={store.loanType}
                  disabled={false}
                  withShadow
                  onChange={value => {
                    if (value && value.value) {
                      if (
                        (value.value !== 'Purchase' &&
                          store.loanType === 'Purchase') ||
                        (value.value === 'Purchase' &&
                          store.loanType !== 'Purchase')
                      ) {
                        store.downPayment = store.homeCost - store.downPayment;
                      }
                      store.loanType = value.value;
                    }
                  }}
                />
              </LoanTypeContainer>
              <div ref={node => (this.purchasePriceRef = node)}>
                <Label>
                  {isPurchase ? 'Purchase Price' : 'Property Value'}
                </Label>
                <InputContainer align="left">
                  <CurrencyInput
                    prefix="$"
                    value={store.homeCost}
                    precision="0"
                    autoFocus={false}
                    onFocus={e => e.preventDefault()}
                    thousandSeparator=","
                    onChangeEvent={(event, maskedvalue, floatvalue) =>
                      (store.homeCost = floatvalue)
                    }
                  />
                </InputContainer>
                {isPurchase && (
                  <LoanPrice error={!loanPrice} width={loanPriceWidth}>
                    <LoanPriceLeft error={!loanPrice} />
                    <div>
                      Loan Amount:{' '}
                      {currencyFormatter.format(loanPrice, {
                        code: 'USD',
                        precision: 0
                      })}
                    </div>
                    <LoanPriceRight error={!loanPrice} />
                  </LoanPrice>
                )}
              </div>
              <div ref={node => (this.downPaymentRef = node)}>
                <Label>{isPurchase ? 'Down Payment' : 'Loan Amount'}</Label>
                <InputContainer align="left">
                  <CurrencyInput
                    prefix="$"
                    value={store.downPayment}
                    precision="0"
                    autoFocus={false}
                    onFocus={e => e.preventDefault()}
                    thousandSeparator=","
                    onChangeEvent={(event, maskedvalue, floatvalue) =>
                      (store.downPayment = floatvalue)
                    }
                  />
                </InputContainer>
              </div>
              <CreditScoreContainer>
                <Label>Credit Score</Label>
                <SelectControl
                  name={'creditScore'}
                  options={creditScoreOptions}
                  validation={() => false}
                  value={store.creditScore}
                  disabled={false}
                  withShadow
                  onChange={value => {
                    if (value.value) store.creditScore = value.value;
                  }}
                />
              </CreditScoreContainer>

              <div>
                <InputContainer align="left">
                  <Label>Zip Code</Label>
                  <input
                    onChange={event => {
                      store.zipCode = event.target.value;
                      store.lookupZipCode(appId);
                    }}
                    pattern="\d*"
                    placeholder="Zip Code"
                    value={store.zipCode}
                  />
                </InputContainer>
                <ZipCode error={!isFormValid}>{zipCodeText}</ZipCode>
              </div>

              <ZipCodeXS error={!isFormValid}>{zipCodeText}</ZipCodeXS>
              <HideOnXS>
                <div style={{ display: 'flex', height: '100%' }}>
                  <SubmitButton
                    type="submit"
                    disabled={!isFormValid || fetchingProducts}
                    fetching={fetchingProducts}
                    red={!isFormValid}
                  >
                    Search
                  </SubmitButton>
                </div>
              </HideOnXS>
            </SearchFormContainer>

            <AdvancedForm>
              <ShowMoreContainer>
                <ShowMoreButton
                  type="button"
                  id="advancedOptionsButton"
                  onClick={this.toggleShowMore}
                >
                  {showMoreOptions ? 'Hide Options' : 'Advanced Options'}
                </ShowMoreButton>
              </ShowMoreContainer>
            </AdvancedForm>
            <AdvancedForm>
              <MoreOptions
                store={store}
                config={config}
                open={showMoreOptions}
                widthForSelect={widthForSelect}
              />
            </AdvancedForm>
            <SearchFormContainerXS>
              <CancelButton onClick={(event) => {
                event.preventDefault();
                this.setState({ showEdit: false });
              }}>Close</CancelButton>
              <SubmitButton
                onClick={() => this.setState({ showEdit: false })}
                type="submit"
                disabled={!isFormValid || fetchingProducts}
                red={!isFormValid}
                fetching={fetchingProducts}
              >
                {fetchingProducts && (
                  <Spinner noLabel color={theme.primaryColor} stroke={3} />
                )}
                Search
              </SubmitButton>
            </SearchFormContainerXS>
          </SearchForm>
          {fetchingProductsError && (
            <ErrorContainer>{fetchingProductsError.message}</ErrorContainer>
          )}
          {_isEmpty(products) &&
            !fetchingProductsError &&
            !fetchingProducts && (
            <NotFound>
              <h1>
                {config.notFoundMsgOne ||
                    'Looks like we need more information on how we can help you.'}
              </h1>
              <h2>
                {config.notFoundMsgTwo ||
                    config.notFoundMessage ||
                    'Would you like some help from a loan specialist?'}
              </h2>

              {config.emailUs || config.notFoundCTA ? (
                <a href={config.notFoundCTA || `mailto:${config.emailUs}`}>
                  <SubmitButton>
                    {config.notFoundButton || 'Email Us'}
                  </SubmitButton>
                </a>
              ) : (
                <React.Fragment>
                  <HideOnXS>
                    <SubmitButton
                      onClick={() => this.setState({ openApply: true })}
                    >
                      {config.notFoundButton || 'Help Me Search'}
                    </SubmitButton>
                  </HideOnXS>
                  <ShowOnXS>
                    <a href={this.addDataToURL(applyPageURL)} target="_blank">
                      <SubmitButton>
                        {config.notFoundButton || 'Help Me Search'}
                      </SubmitButton>
                    </a>
                  </ShowOnXS>
                </React.Fragment>
              )}
            </NotFound>
          )}
          {!_isEmpty(products) && this.sortProducts(products, sortedFields)}
        </Content>
      </Page>
    );
  }
}

export default observer(SearchPage);
