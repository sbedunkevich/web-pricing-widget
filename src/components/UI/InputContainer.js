import styled from 'styled-components';

const InputContainer = styled.div`
  text-align: ${props => (props.align ? props.align : 'center')};
  input {
    padding: 0px 9px;
    border: 1px solid #ddd;
    border-radius: 6px !impornant;
    outline: none;
    height: 40px;
    font-size: 16px;
    width: 100%;
    transition: all 0.4s ease;
    box-sizing: border-box;
    appearance: none;
    font-family: ${props => props.theme.fontFamily};
    &:focus {
      border-color: ${props => props.theme.primaryColor};
      box-shadow: 0 0 2px 0 ${props => props.theme.primaryColor};
    }
  }
`;

export default InputContainer;
