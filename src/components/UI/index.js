import styled from 'styled-components';
import InputContainer from './InputContainer';
import { media } from 'web-ui-components';

const Page = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: none;
`;

const Content = styled.div`
  width: auto;

  ${media.tablet`
    width: calc(100% - 20px);
  `} ${media.mobile`
    width: 90%;
  `};
`;

const Header1 = styled.div`
  font-size: 35px;
  font-weight: 500;
  margin-top: 0;

  ${media.mobile`
    text-align: center;
  `}
`;

const Header2 = styled.div`
  text-align: center;
  font-weight: 200;
  font-size: 26px;
  margin-bottom: 1em;
  ${media.mobile`
    font-size: 18px;
  `};
`;

const ApplyButton = styled.button`
  background-color: ${props => props.theme.primaryColor};
  color: white;
  border-radius: 6px;
  padding: 5px 10px;
  font-size: 16px;
  outline: none !important;
  border: none;
  height: 39px;
  cursor: pointer;
`;

const BigApplyButton = styled(ApplyButton)`
  padding: 15px 60px;
  height: auto;
  font-size: 18px;
`;

const HideOnXS = styled.span`
  display: block;
  ${media.mobile`
    display: none !important;
  `};
`;

const ShowOnXS = styled.span`
  display: none;
  ${media.mobile`
    display: block !important;
  `};
`;

export {
  Page,
  Content,
  InputContainer,
  Header1,
  Header2,
  ApplyButton,
  BigApplyButton,
  HideOnXS,
  ShowOnXS
};
