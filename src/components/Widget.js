import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import {
  PopUp,
  ProgressBar,
  Button,
  TextButton,
  media
} from 'web-ui-components';
import ModalPage from './ModalPage';
import { observer } from 'mobx-react';
import {
  STATUS_OK,
  STATUS_ERROR,
  FIRST_MODAL_PROGRESS_BAR_HEIGHT
} from './const';
import SearchPage from './SearchPage';
import searchRequestInformation from '../api/request.json';
import Chevron from './UI/chevron.svg';

const MainContainer = styled.div`
  font-family: ${props => props.theme.fontFamily};
`;

const Footer = styled.div`
  border-top: 1px solid #eee;
  padding: 30px;
  display: flex;
  flex-direction: row-reverse;
  button {
    outline: none !important;
  }
  ${media.mobile`
    padding: 10px;
  `};
`;

const Container = styled.div`
  padding: 20px;
  padding-top: 40px;

  ${media.mobile`
    padding: 10px;
  `};
`;

const Error = styled.div`
  font-size: 13px;
  font-weight: 500;
  text-align: center;
  color: #bbb;
  min-height: 20px;
  margin-bottom: 1em;
  transition: all 0.5s ease;
  opacity: ${props => (props.hasError ? '1' : '0')};
  ${media.mobile`
    font-size: 12px;
    min-height: 15px;
    margin-bottom: 5px;
  `};
`;

const BackButtonContainer = styled.div`
  flex: 1;
`;

const LeftArrow = styled.div`
  width: 16px;
  height: 16px;
  background: url(${Chevron}) top left no-repeat;
  transform: rotate(180deg);
  margin-right: 17px;
  display: inline-block;
`;

class Widget extends Component {
  static propTypes = {
    unmount: PropTypes.func.isRequired,
    theme: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired
  };

  state = {
    pageIndex: 0,
    pages: ModalPage,
    error: null
  };

  close = () => {
    this.props.unmount();
  };

  goNext = () => {
    const { pageIndex, pages } = this.state;
    const { store } = this.props;

    if (pages[pageIndex].cantGoNext) {
      const result = pages[pageIndex].cantGoNext(store);
      const { status, message } = result;

      switch (status) {
        case STATUS_ERROR:
          this.setState({ error: message });
          setTimeout(() => this.setState({ error: null }), 3000);
          return;
        case STATUS_OK:
          this.setState({ error: null });
          break;
        default:
          return;
      }
    }

    if (pageIndex < pages.length - 1) {
      this.setState({ pageIndex: pageIndex + 1 });
    }
  };

  goBack = () => {
    const { pageIndex } = this.state;
    if (pageIndex >= 1) {
      this.setState({ pageIndex: pageIndex - 1 });
    }
  };

  searchForResult = async event => {
    const { store, config } = this.props;
    const { appId } = config;
    const { isZipCodeValid, loanPrice } = store;

    if (event) event.preventDefault();

    if (!isZipCodeValid || !loanPrice) return;

    searchRequestInformation.BorrowerInformation.FICO = store.creditScore;
    searchRequestInformation.RepresentativeFICO = store.creditScore;

    searchRequestInformation.BorrowerInformation.SelfEmployed =
      store.selfEmployed;
    searchRequestInformation.BorrowerInformation.FirstTimeHomeBuyer =
      store.firstTimeHomeBuyer;
    searchRequestInformation.BorrowerInformation.Bankruptcy = store.bankruptcy
      ? 305
      : 'Never';
    searchRequestInformation.BorrowerInformation.Foreclosure = store.foreclosure
      ? 297
      : 'Never';
    searchRequestInformation.BorrowerInformation.typeOfVeteran =
      store.militaryVeteran === 'N/A' ? null : store.militaryVeteran;

    searchRequestInformation.PropertyInformation.SalesPrice = store.homeCost;
    searchRequestInformation.PropertyInformation.ZipCode = store.zipCode;
    searchRequestInformation.PropertyInformation.County = store.zipData.city;
    searchRequestInformation.PropertyInformation.State = store.zipData.state;
    searchRequestInformation.PropertyInformation.PropertyType =
      store.propertyType;
    searchRequestInformation.PropertyInformation.properties = {
      occupancy: store.propertyUse
    };

    if (store.loanType === 'Purchase') {
      searchRequestInformation.LoanInformation.BaseLoanAmount =
        store.homeCost - store.downPayment;
    } else {
      searchRequestInformation.LoanInformation.BaseLoanAmount =
        store.downPayment;
    }

    searchRequestInformation.LoanInformation.LoanPurpose = store.loanType;

    searchRequestInformation.LoanInformation.LoanTerms =
      store.selectedLoanTerms;

    localStorage.setItem(
      'last_search',
      JSON.stringify(searchRequestInformation)
    );

    await store.searchResults(appId, searchRequestInformation);
  };

  renderForm = store => {
    const { config } = this.props;
    const { theme } = config;
    const { pageIndex, pages, error } = this.state;
    const PageComponent = pages[pageIndex];

    return (
      <PopUp
        poped={true}
        closePopup={this.close}
        noPadding
        title={`Step ${pageIndex + 1}/${pages.length}`}
      >
        <ProgressBar
          value={pageIndex + 1}
          animationTime={0.7}
          animationType="linear"
          colors={
            store.animateLoader
              ? [
                theme.gradientFromColor,
                theme.gradientToColor,
                theme.gradientFromColor
              ]
              : [theme.gradientFromColor, theme.gradientToColor]
          }
          animated={store.animateLoader}
          total={pages.length}
          height={FIRST_MODAL_PROGRESS_BAR_HEIGHT}
        />
        <Container>
          <PageComponent
            store={store}
            config={config}
            searchForResult={this.searchForResult}
          />
        </Container>
        <Error hasError={!!error}>{error}</Error>
        <Footer>
          {pageIndex < pages.length - 1 && (
            <Button id="nextButton" onClick={this.goNext}>
              Next
            </Button>
          )}
          {!!pageIndex && (
            <BackButtonContainer>
              <TextButton onClick={this.goBack}>
                <LeftArrow />Back
              </TextButton>
            </BackButtonContainer>
          )}
        </Footer>
      </PopUp>
    );
  };

  render() {
    const { theme, store, config } = this.props;

    return (
      <ThemeProvider theme={theme}>
        <MainContainer>
          {store.signedIn ? (
            <SearchPage
              store={store}
              config={config}
              searchForResult={this.searchForResult}
            />
          ) : (
            this.renderForm(store)
          )}
        </MainContainer>
      </ThemeProvider>
    );
  }
}

export default observer(Widget);
