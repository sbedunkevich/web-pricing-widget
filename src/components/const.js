const STATUS_OK = 'STATUS_OK';
const STATUS_ERROR = 'STATUS_ERROR';

const FIRST_MODAL_PROGRESS_BAR_HEIGHT = '15px';

const LOCAL_STORAGE_STATE = 'mobx_store_data';
const LOCAL_STORAGE_DATA_VERION_NAME = 'ls_data_version';

const getLocalStorageDataVersion = () => {
  try {
    const url = `${WEBPACK_LOCAL_STORAGE_DATA_VERION}`; // eslint-disable-line
    return url;
  } catch (err) {
    return '0.0.3';
  }
};

const LOCAL_STORAGE_REDIRECT_BACK_URL = 'redirect_back_url';
const LOCAL_STORAGE_REDIRECT_PAGE_URL = 'redirect_page_url';

const SORTED_FIELDS = [
  { name: 'payment', field: 'totalPayment' },
  { name: 'rate', field: 'rate' },
  { name: 'apr', field: 'apr' },
  { name: 'fees', field: 'closingCost' }
];

// finite state machine status implementation
const STATUS = {
  1: { sorted: false, order: null, next: 2, humanValue: '' },
  2: { sorted: true, order: 'asc', next: 3, humanValue: 'ascending' },
  3: { sorted: true, order: 'desc', next: 2, humanValue: 'descending' }
};

const defaultLoanTerms = [
  { name: 'Select All', value: false, selected: false },
  { name: '30 Year Fixed', value: 'ThirtyYear', selected: false },
  { name: '25 Year Fixed', value: 'TwentyFiveYear', selected: false },
  { name: '20 Year Fixed', value: 'TwentyYear', selected: false },
  { name: '15 Year Fixed', value: 'FifteenYear', selected: false },
  /* {
    name: 'Amort Type - Fixed',
    value: 'sProdFilterFinMethFixed',
    selected: false
  },*/
  { name: '3/1 ARM', value: 'sProdFilterFinMeth3YrsArm', selected: false },
  { name: '5/1 ARM', value: 'sProdFilterFinMeth5YrsArm', selected: false },
  { name: '7/1 ARM', value: 'sProdFilterFinMeth7YrsArm', selected: false },
  { name: '10/1 ARM', value: 'sProdFilterFinMeth10YrsArm', selected: false }
  /* {
    name: 'Product Type - Conventional',
    value: 'sProdIncludeNormalProc',
    selected: false
  },
  {
    name: 'Product Type - My Community',
    value: 'sProdIncludeMyCommunityProc',
    selected: false
  },
  {
    name: 'Product Type - Home Possible',
    value: 'sProdIncludeHomePossibleProc',
    selected: false
  },
  {
    name: 'Product Type - FHA',
    value: 'sProdIncludeFHATotalProc',
    selected: false
  },
  { name: 'Product Type - VA', value: 'sProdIncludeVAProc', selected: false },
  { name: 'Impound', value: 'sProdImpound', selected: false }*/
];

const creditScoreOptions = [
  {
    value: 770,
    label: '750+ (Excellent)',
    disabled: false
  },
  {
    value: 730,
    label: '700-749 (Good)',
    disabled: false
  },
  {
    value: 670,
    label: '650-699 (Fair)',
    disabled: false
  },
  {
    value: 580,
    label: '550-649 (Poor)',
    disabled: false
  }
];

const loanTypeOptions = [
  {
    value: 'Purchase',
    label: 'Purchase',
    disabled: false
  },
  {
    value: 'Refi',
    label: 'Refinance',
    disabled: false
  }
  /* {
    value: 'RefiCashout',
    label: 'Cash-Out Refinance',
    disabled: false
  }
  {
    value: 'FHA Streamline Refi',
    label: 'FHA Streamline Refi',
    disabled: false
  },
  {
    value: 'VA IRRRL',
    label: 'VA IRRRL',
    disabled: false
  }*/
];

const LOAN_TYPE_PURCHASE = loanTypeOptions[0].value;
const LOAN_TYPE_REFI = loanTypeOptions[1].value;

const propertyTypeOptions = [
  {
    value: 'SingleFamily',
    label: 'Single Family Home',
    disabled: false
  },
  {
    value: 'Condo',
    label: 'Condo',
    disabled: false
  },
  /* {
    value: 'Condotel',
    label: 'Condotel',
    disabled: false
  },*/
  {
    value: 'Modular',
    label: 'Modular',
    disabled: false
  },
  {
    value: 'PUD',
    label: 'PUD',
    disabled: false
  },
  /* {
    value: 'Timesharer',
    label: 'Timesharer',
    disabled: false
  },*/
  {
    value: 'ManufacturedSingleWide',
    label: 'Manufactured Single Wide',
    disabled: false
  },
  {
    value: 'ManufacturedDoubleWide',
    label: 'Manufactured Double Wide',
    disabled: false
  },
  {
    value: 'Coop',
    label: 'Coop',
    disabled: false
  }
  /* {
    value: 'NonWarrantableCondo',
    label: 'NonWarrantableCondo',
    disabled: false
  },
  {
    value: 'Townhouse',
    label: 'Townhouse',
    disabled: false
  }*/
];

const propertyUseOptions = [
  {
    value: 'PrimaryResidence',
    label: 'Primary Residence',
    disabled: false
  },
  {
    value: 'SecondHome',
    label: 'Second Home',
    disabled: false
  },
  {
    value: 'InvestmentProperty',
    label: 'Investment Property',
    disabled: false
  }
];

const militaryVeteranOptions = [
  {
    value: 'N/A',
    label: 'Non Military',
    disabled: false
  },
  {
    value: 'ActiveDuty',
    label: 'Active Duty',
    disabled: false
  },
  {
    value: 'NationalGuardOrReserves',
    label: 'National Guard Or Reserves',
    disabled: false
  }
];

export {
  FIRST_MODAL_PROGRESS_BAR_HEIGHT,
  LOAN_TYPE_PURCHASE,
  LOAN_TYPE_REFI,
  LOCAL_STORAGE_REDIRECT_BACK_URL,
  LOCAL_STORAGE_REDIRECT_PAGE_URL,
  LOCAL_STORAGE_STATE,
  getLocalStorageDataVersion,
  LOCAL_STORAGE_DATA_VERION_NAME,
  SORTED_FIELDS,
  STATUS,
  STATUS_ERROR,
  STATUS_OK,
  creditScoreOptions,
  defaultLoanTerms,
  loanTypeOptions,
  militaryVeteranOptions,
  propertyTypeOptions,
  propertyUseOptions
};
