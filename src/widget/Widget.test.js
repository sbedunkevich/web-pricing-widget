import React from 'react';
import ReactDOM from 'react-dom';
import Widget from '../components/Widget';
import DecoratedStore from './store';
import { mount, shallow } from 'enzyme';
import { creditScoreOptions } from '../components/const';
import * as Api from '../api';

Api.searchResults = jest.fn();

const store = new DecoratedStore();
const Unmount = () => true;
const StreamLoanConfig = {
  appId: 'acme2',
  elementSelector: '.open_widget_button',
  redirectPageURL: '/search-loan-page',
  applyPageURL:
    'https://master-myacme.streamloan.io/invite.html?id=9b71affac6736b52925d80e5a1560d1b657597d0',
  theme: {
    primaryColor: '#3498db',
    secondaryColor: '#3498dc',
    fontFamily: 'Lato, Google Sans, Lato, Arial',
    gradientFromColor: 'white',
    gradientToColor: '#3498db'
  }
};

it('Widget renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <Widget
      unmount={Unmount}
      theme={StreamLoanConfig.theme}
      store={store}
      config={StreamLoanConfig}
    />,
    div
  );

  ReactDOM.unmountComponentAtNode(div);
});

describe('Widget Tests', () => {
  const wrapper = mount(
    <Widget
      unmount={Unmount}
      theme={StreamLoanConfig.theme}
      store={store}
      config={StreamLoanConfig}
    />
  );

  it('Should pass AskForLoanType', async () => {
    const AskForLoanType = shallow(wrapper.find('AskForLoanType').get(0));
    const zipCodeInput = AskForLoanType.find('t');
    const nextButton = shallow(wrapper.find('#nextButton').get(0));

    await zipCodeInput.prop('onFieldValueChange')('90210');

    expect(store.zipData).toMatchObject({
      zip: '90210',
      latitude: 34.0901,
      longitude: -118.4065,
      city: 'Beverly Hills',
      state: 'CA',
      country: 'US'
    });

    nextButton.simulate('click');
    wrapper.update();
  });

  it('Should pass NewHomeCost', async () => {
    const NewHomeCost = shallow(wrapper.find('NewHomeCost').get(0));
    const nextButton = shallow(wrapper.find('#nextButton').get(0));
    const input = NewHomeCost.find('c');
    input.prop('onChangeEvent')(null, null, 350000.0);

    expect(store.homeCost).toBe(350000.0);

    nextButton.simulate('click');
    wrapper.update();
  });

  it('Should pass DownPayment', async () => {
    const DownPayment = shallow(wrapper.find('DownPayment').get(0));
    const nextButton = shallow(wrapper.find('#nextButton').get(0));
    const input = DownPayment.find('c');

    input.prop('onChangeEvent')(null, null, 50000.0);

    expect(store.downPayment).toBe(50000.0);

    nextButton.simulate('click');
    wrapper.update();
  });

  it('Should pass CreditScore', async () => {
    const CreditScore = shallow(wrapper.find('CreditScore').get(0));
    const nextButton = shallow(wrapper.find('#nextButton').get(0));

    expect(store.creditScore).toBe(creditScoreOptions[0].value);
    nextButton.simulate('click');
    wrapper.update();
  });

  it('Should pass AskEmail', async () => {
    const AskEmail = shallow(wrapper.find('AskEmail').get(0));

    expect(store.fetchingProducts).toBe(true);
  });

  it('Should render SearchPage and toggle show more options', async () => {
    store.email = 'any@email.com';
    store.signedIn = true;
    wrapper.update();

    const SearchPage = shallow(wrapper.find('SearchPage').get(0));

    SearchPage.find('#advancedOptionsButton').simulate('click');
    expect(SearchPage.state('showMoreOptions')).toBe(true);
  });
});
