import {
  decorate,
  observable,
  action,
  configure,
  computed,
  autorun,
  runInAction
} from 'mobx';
import {
  LOAN_TYPE_PURCHASE,
  LOAN_TYPE_REFI,
  LOCAL_STORAGE_STATE,
  LOCAL_STORAGE_REDIRECT_BACK_URL,
  LOCAL_STORAGE_REDIRECT_PAGE_URL,
  creditScoreOptions
} from '../components/const';
import * as API from '../api';

const NOT_SERVED = 'NOT_SERVED';
const NOT_VALID = 'NOT_VALID';

//
// Configure MobX store
// enforceActions: true – only actions can change the state
//
configure({ isolateGlobalState: true, enforceActions: false });

class Store {
  initialData = JSON.parse(localStorage.getItem(LOCAL_STORAGE_STATE)) || {
    bankruptcy: false,
    creditScore: creditScoreOptions[0].value,
    downPayment: '',
    email: '',
    firstTimeHomeBuyer: false,
    foreclosure: false,
    homeCost: '',
    loanType: LOAN_TYPE_PURCHASE,
    militaryVeteran: 'N/A',
    propertyType: 'SingleFamily',
    propertyUse: 'PrimaryResidence',
    selectedLoanTerms: [
      'ThirtyYear',
      'TwentyFiveYear',
      'TwentyYear',
      'FifteenYear'
    ],
    selfEmployed: false,
    signedIn: false,
    zipCode: '',
    zipData: ''
  };

  bankruptcy = this.initialData.bankruptcy;
  creditScore = this.initialData.creditScore;
  downPayment = this.initialData.downPayment;
  email = this.initialData.email;
  firstTimeHomeBuyer = this.initialData.firstTimeHomeBuyer;
  foreclosure = this.initialData.foreclosure;
  homeCost = this.initialData.homeCost;
  loanType = this.initialData.loanType;
  militaryVeteran = this.initialData.militaryVeteran;
  propertyType = this.initialData.propertyType;
  propertyUse = this.initialData.propertyUse;
  selectedLoanTerms = this.initialData.selectedLoanTerms;
  selfEmployed = this.initialData.selfEmployed;
  signedIn = this.initialData.signedIn;
  zipCode = this.initialData.zipCode;
  zipData = { ...this.initialData.zipData };

  animateLoader = false;
  fetchingProducts = false;
  fetchingProductsError = false;
  forseSearch = false;
  products = [];
  ineligible = [];
  searchId = '';
  selectedLoanTermsHumanized = '';
  signUpStatus = '';
  state = '';
  redirectPageURL = null;

  get isEmailValid() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line
    return re.test(String(this.email).toLowerCase());
  }

  get isZipCodeValid() {
    return !!this.zipData.state;
  }

  get isZipCodeServed() {
    return this.zipData.state && this.zipData.state !== NOT_SERVED;
  }

  get loanPrice() {
    const loanPrice = this.homeCost - this.downPayment;
    return loanPrice > 0 ? loanPrice : 0;
  }

  logOut() {
    const backUrl = localStorage.getItem(LOCAL_STORAGE_REDIRECT_BACK_URL);
    localStorage.removeItem(LOCAL_STORAGE_STATE);
    window.location = backUrl;
  }

  async signUp() {
    this.signUpStatus = 'pending';
    try {
      runInAction(() => {
        this.signUpStatus = 'success';
        this.signedIn = true;

        localStorage.setItem(
          LOCAL_STORAGE_REDIRECT_BACK_URL,
          window.location.href
        );

        const redirectPageURL = localStorage.getItem(
          LOCAL_STORAGE_REDIRECT_PAGE_URL
        );
        if (redirectPageURL && window.location.pathname !== redirectPageURL) {
          window.location = redirectPageURL;
        }
      });
    } catch (error) {
      runInAction(() => {
        this.signUpStatus = 'error';
      });
    }
  }

  async setZipCode(appId, zipCode) {
    this.zipCode = zipCode;
    await this.lookupZipCode(appId);
    if (this.isZipCodeValid && this.isZipCodeServed) {
      this.forseSearch = true;
    }
  }

  updateSearchURL(config) {
    const { redirectPageURL } = config;

    if (
      redirectPageURL &&
      window.location.pathname.search(redirectPageURL) !== -1
    ) {
      this.redirectPageURL = redirectPageURL;
    }
  }

  async lookupZipCode(appId) {
    if (this.zipCode.length !== 5) {
      runInAction(() => {
        this.zipData = {};
      });
      return;
    }
    try {
      const zipData = await API.checkZipcode(appId, this.zipCode);

      runInAction(() => {
        this.zipData = zipData;
      });
    } catch (error) {
      const { status } = error;

      if (status === NOT_VALID) {
        runInAction(() => {
          this.zipData = {};
        });
      }

      if (status === NOT_SERVED) {
        runInAction(() => {
          this.zipData = { city: NOT_SERVED, state: NOT_SERVED };
        });
      }
    }
  }

  async getDetails(appId, searchId, productId, callback) {
    API.getDetails(appId, searchId, productId).then(() => {
      // console.log("getDetails", data); // eslint-disable-line
      callback();
    });
  }

  async searchResults(appId, borrowerInformation) {
    // this.products = [];
    this.fetchingProducts = true;
    this.fetchingProductsError = false;

    try {
      const data = await API.searchResults(appId, borrowerInformation);

      const { searchId, products, ineligible } = data.body;
      const { statusCode } = data;

      if (statusCode === 400) {
        throw new Error('The request is invalid.');
      }

      // after await, modifying state again, needs an actions:

      runInAction(() => {
        this.searchId = searchId;
        this.products = products;
        this.ineligible = ineligible;
        this.fetchingProducts = false;
        this.fetchingProductsError = false;
      });
    } catch (error) {
      runInAction(() => {
        this.fetchingProducts = false;
        this.fetchingProductsError = { message: error.message };
      });
    }
  }

  //
  // Save changes to localStorage
  //
  reaction = autorun(() => {
    const data = {
      bankruptcy: this.bankruptcy,
      creditScore: this.creditScore,
      downPayment: this.downPayment,
      email: this.email,
      firstTimeHomeBuyer: this.firstTimeHomeBuyer,
      foreclosure: this.foreclosure,
      homeCost: this.homeCost,
      loanType: this.loanType,
      militaryVeteran: this.militaryVeteran,
      propertyType: this.propertyType,
      propertyUse: this.propertyUse,
      selectedLoanTerms: this.selectedLoanTerms,
      selfEmployed: this.selfEmployed,
      signedIn: this.signedIn,
      zipCode: this.zipCode,
      zipData: this.zipData
    };

    // ToDo in Production

    if (this.redirectPageURL) {
      const urlParams = new URLSearchParams(window.location.search);

      if (
        urlParams.get('downPayment') &&
        urlParams.get('homeCost') &&
        urlParams.get('zipCode') &&
        urlParams.get('loanType')
      ) {
        let loanType;
        switch (this.loanType) {
          case LOAN_TYPE_REFI:
            loanType = 'refi';
            break;
          default:
            loanType = 'purchase';
            break;
        }

        history.replaceState(
          {},
          document.title,
          `?homeCost=${this.homeCost}&downPayment=${
            this.downPayment
          }&creditScore=${this.creditScore}&zipCode=${this.zipCode ||
            'NA'}&loanType=${loanType}&firstTimeHomeBuyer=${
            this.firstTimeHomeBuyer
          }&propertyType=${this.propertyType}&propertyUse=${
            this.propertyUse
          }&militaryVeteran=${this.militaryVeteran}&selectedLoanTerms=${escape(
            this.selectedLoanTerms
          )}`
        );
      }
    }
    localStorage.setItem(LOCAL_STORAGE_STATE, JSON.stringify(data));
  });
}

const DecoratedStore = decorate(Store, {
  //
  // observable store values
  //
  animateLoader: observable,
  bankruptcy: observable,
  creditScore: observable,
  downPayment: observable,
  email: observable,
  fetchingProducts: observable,
  fetchingProductsError: observable,
  firstTimeHomeBuyer: observable,
  foreclosure: observable,
  homeCost: observable,
  loanType: observable,
  militaryVeteran: observable,
  products: observable,
  ineligible: observable,
  propertyType: observable,
  propertyUse: observable,
  searchId: observable,
  selectedLoanTerms: observable,
  selectedLoanTermsHumanized: observable,
  selfEmployed: observable,
  signUpStatus: observable,
  signedIn: observable,
  state: observable,
  zipCode: observable,
  zipData: observable,
  forseSearch: observable,
  //
  // computed store values
  //
  isEmailValid: computed,
  isZipCodeValid: computed,
  loanPrice: computed,
  isZipCodeServed: computed,
  //
  // actions
  //
  getDetails: action,
  logOut: action,
  lookupZipCode: action,
  searchResults: action,
  signUp: action,
  setZipCode: action,
  updateSearchURL: action
});

export default DecoratedStore;
