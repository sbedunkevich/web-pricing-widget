import 'url-search-params-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Widget from '../components/Widget';
import DecoratedStore from './store';
import { getOptions } from '../api';
import {
  LOCAL_STORAGE_DATA_VERION_NAME,
  getLocalStorageDataVersion,
  LOCAL_STORAGE_STATE,
  LOAN_TYPE_PURCHASE,
  LOAN_TYPE_REFI
} from '../components/const';

let StreamLoanConfig = null;
const store = new DecoratedStore();

const toSlug = str =>
  str
    .split(' ')
    .join('-')
    .toLowerCase();

const Unmount = () => {
  ReactDOM.unmountComponentAtNode(
    document.getElementById(StreamLoanConfig.widgetContainerId)
  );
};

const Init = {
  init: async config => {
    //
    // Widget configuration
    //
    StreamLoanConfig = config;
    let serverOptions = { theme: {} };
    const { appId, widgetContainerId } = StreamLoanConfig;
    let { elementSelector } = StreamLoanConfig;

    try {
      const { options } = await getOptions(appId);
      const json = JSON.parse(options);

      if (json) {
        serverOptions = json;
      }
      if (serverOptions.elementSelector) {
        elementSelector = serverOptions.elementSelector;
      }
      if (!StreamLoanConfig.applyPageURL && !serverOptions.applyPageURL) {
        serverOptions.applyPageURL = '/';
      }
    } catch (err) {
      console.warn(err);
    }

    if (widgetContainerId && !document.getElementById(widgetContainerId)) {
      console.warn(`StreamLoan: Can't find container ${widgetContainerId}`); //eslint-disable-line
      StreamLoanConfig.widgetContainerId = toSlug(
        `${appId}-stream-loan-widget`
      );
    }
    if (!widgetContainerId) {
      StreamLoanConfig.widgetContainerId = toSlug(
        `${appId}-stream-loan-widget`
      );
    }

    if (!document.querySelector(elementSelector)) {
      console.warn(`StreamLoan: Can't find element ${elementSelector}`); //eslint-disable-line
    }
    //
    // Create the container for widget render
    //
    const container = document.createElement('div');
    container.setAttribute('id', StreamLoanConfig.widgetContainerId);
    container.setAttribute('style', 'all: initial;');

    document.body.appendChild(container);

    console.log(`You are using ${React.version} version of React`); //eslint-disable-line
    console.log("StreamLoanConfig init..."); //eslint-disable-line

    let shouldCheckForDataVersion = true;

    // Preset data from URL
    const urlParams = new URLSearchParams(window.location.search);
    const downPayment = urlParams.get('downPayment');
    const homeCost = urlParams.get('homeCost');
    const zipCode = urlParams.get('zipCode');
    const loanType = urlParams.get('loanType');

    if (downPayment && homeCost && zipCode && loanType) {
      const propertyType = urlParams.get('propertyType');
      const propertyUse = urlParams.get('propertyUse');
      const militaryVeteran = urlParams.get('militaryVeteran');
      const selectedLoanTerms = urlParams.get('selectedLoanTerms');
      const creditScore = urlParams.get('creditScore');
      let newLoanType = LOAN_TYPE_PURCHASE;

      store.downPayment = downPayment;
      store.homeCost = homeCost;
      switch (loanType) {
        case 'purchase':
          newLoanType = LOAN_TYPE_PURCHASE;
          break;
        case 'refi':
          newLoanType = LOAN_TYPE_REFI;
          break;
        default:
          break;
      }
      store.loanType = newLoanType;
      store.setZipCode(appId, zipCode);

      if (propertyType) {
        store.propertyType = propertyType;
      }
      if (propertyUse) {
        store.propertyUse = propertyUse;
      }
      if (militaryVeteran) {
        store.militaryVeteran = militaryVeteran;
      }

      if (selectedLoanTerms) {
        store.selectedLoanTerms = selectedLoanTerms.split(',');
      }

      if (creditScore) {
        store.creditScore = creditScore;
      }

      const firstTimeHomeBuyer = urlParams.get('firstTimeHomeBuyer');
      switch (firstTimeHomeBuyer) {
        case 'true':
          store.firstTimeHomeBuyer = true;
          break;
        case 'false':
          store.firstTimeHomeBuyer = false;
          break;
        default:
          break;
      }

      store.signedIn = true;
      shouldCheckForDataVersion = false;
    }

    if (shouldCheckForDataVersion) {
      const lsDataVersion = localStorage.getItem(
        LOCAL_STORAGE_DATA_VERION_NAME
      );

      if (lsDataVersion !== getLocalStorageDataVersion()) {
        localStorage.removeItem(LOCAL_STORAGE_STATE);
        localStorage.setItem(
          LOCAL_STORAGE_DATA_VERION_NAME,
          getLocalStorageDataVersion()
        );

        store.signedIn = false;
        // window.location = '/';
        // return;
      }
    }

    // Default colors theme
    const theme = {
      primaryColor: 'blue',
      secondaryColor: 'blue',
      placeHolderColor: '#ccc',
      gradientFromColor: 'blue',
      gradientToColor: 'green',
      fontFamily: 'Arial'
    };

    const serverOptionsTheme = serverOptions.theme || {};
    delete (serverOptions.theme);
    StreamLoanConfig.theme = { ...theme, ...StreamLoanConfig.theme, ...serverOptionsTheme };
    StreamLoanConfig = { ...StreamLoanConfig, ...serverOptions };

    if (!StreamLoanConfig.phoneNumber) {
      StreamLoanConfig.phoneNumber = null;
    }

    if (!StreamLoanConfig.showDisclamer) {
      StreamLoanConfig.showDisclamer = false;
    }

    if (StreamLoanConfig.redirectPageURL) {
      localStorage.setItem(
        'redirect_page_url',
        StreamLoanConfig.redirectPageURL
      );
    }

    //
    // If location path is the result page render widget instantly
    //
    if (
      store.signedIn &&
      StreamLoanConfig.redirectPageURL &&
      window.location.pathname.search(StreamLoanConfig.redirectPageURL) !== -1
    ) {
      ReactDOM.render(
        <Widget
          unmount={Unmount}
          theme={StreamLoanConfig.theme}
          store={store}
          config={StreamLoanConfig}
        />,
        document.getElementById(StreamLoanConfig.widgetContainerId)
      );
      return;
    }

    //
    // Add click listener to the button to invoke widget for the first time
    //

    const nodesArray = [].slice.call(
      document.querySelectorAll(elementSelector)
    );

    nodesArray.forEach(elem =>
      elem.addEventListener('click', () => {
        if (
          store.signedIn &&
          StreamLoanConfig.redirectPageURL &&
          window.location.pathname !== StreamLoanConfig.redirectPageURL
        ) {
          window.location = StreamLoanConfig.redirectPageURL;
        } else {
          ReactDOM.render(
            <Widget
              unmount={Unmount}
              theme={StreamLoanConfig.theme}
              store={store}
              config={StreamLoanConfig}
            />,
            document.getElementById(StreamLoanConfig.widgetContainerId)
          );
        }
      })
    );
  },

  unmount: Unmount
};

export default Init;
