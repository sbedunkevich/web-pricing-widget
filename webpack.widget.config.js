/* global __dirname, require, module*/

const webpack = require('webpack');
const { UnusedFilesWebpackPlugin } = require('unused-files-webpack-plugin');
const path = require('path');
const args = require('yargs').argv;
const { mode } = args;
const pkg = require('./package.json');

let libraryVersion = pkg.version;
let localStorageDataVersion = pkg.localStorageDataVersion;
let libraryName = 'StreamLoan';
let plugins = [];
let outputFile;
let outputDir;
let optimization;
let devtool;

plugins.push(
  new webpack.DefinePlugin({
    WEBPACK_LOCAL_STORAGE_DATA_VERION: JSON.stringify(localStorageDataVersion)
  })
);

if (mode === 'production') {
  outputDir = '/build';
  devtool = false;
  outputFile = libraryName.toLocaleLowerCase() + '.min.js';
  optimization = {};
  plugins.push(
    new webpack.BannerPlugin(
      `Copyright (c) 2018 StreamLoan Inc. v${libraryVersion}
UNLICENSED`
    )
  );
  plugins.push(
    new webpack.DefinePlugin({
      CONFIG_API_URL: JSON.stringify(
        process.env.API_URL || 'https://dpricing.streamloan.io'
      )
    })
  );
} else {
  devtool = 'source-map';
  outputDir = '/public/lib';
  outputFile = libraryName.toLocaleLowerCase() + '.js';
  optimization = {};
  plugins.push(new UnusedFilesWebpackPlugin({ patterns: 'src/*.*' }));
  plugins.push(
    new webpack.DefinePlugin({
      CONFIG_API_URL: JSON.stringify('http://localhost:3000')
    })
  );
}

const config = {
  entry: ['babel-polyfill', __dirname + '/src/widget/widget.js'],
  devtool: devtool,
  output: {
    path: __dirname + outputDir,
    filename: outputFile,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  optimization: optimization,
  module: {
    rules: [
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        use: {
          loader: 'svg-url-loader'
        }
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'babel-loader',
        query: {
          plugins: ['lodash', 'add-module-exports'],
          presets: [['env'], 'react-app']
        },
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    modules: [path.resolve('./node_modules'), path.resolve('./src')],
    extensions: ['.json', '.js']
  },
  plugins: plugins
};

module.exports = config;
